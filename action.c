#include <fcntl.h>
#include <string.h>

#include "grpm.h"
#include "misc.h"

#define GTK_FLUSH \
    while (gtk_events_pending()) \
	gtk_main_iteration();
/*#define TESTING 1*/

static rpmDependencies installDeps;

static gint grpmActionGetConflicts(GrpmActionList *action,
				   rpmDependencies *rpmdep,
				   struct rpmDependencyConflict **conflicts,
				   int *numConflicts);
static void grpmActionResolveDeps(GrpmActionList *action,
				  rpmDependencies rpmdep,
				  struct rpmDependencyConflict *conflicts,
				  int numConflicts);

static void grpmActionResolveDepsRemoveCB(GtkWidget *button, GtkCList *clist);
static void grpmActionFinishCheckDepsCB(GtkWidget *button, GtkCList *clist);

static void grpmActionDoInstall(GrpmActionList *action);
static void grpmActionInstallCB(gint reply, GrpmActionList *action);
static gint grpmActionUninstallAux(GrpmActionList *action,
				   rpmdb *db,
				   GtkLabel *label, GtkLabel *name,
				   GrpmPackage *pkg);
static gint grpmActionInstallAux(GrpmActionList *action,
				 rpmdb *db,
				 GtkLabel *label, GtkLabel *name,
				 GrpmPackage *pkg);
static void grpmActionDoInstallCancelCB(GtkWidget *button, gint *cancelled);
static void grpmActionUpdatePkgProgress(const unsigned long amount,
					const unsigned long total);

GrpmActionList *grpmActionNew(void)
{
    GrpmActionList *res;

    res = g_new(GrpmActionList, 1);
    res->list = grpmPackageListNew();

    return res;
}

void grpmActionDestroy(GrpmActionList *action)
{
    grpmPackageListFree(action->list, FALSE);

    g_free(action);
}

void grpmActionAddInstallPackage(GrpmActionList *action,
				 GrpmPackageListItem *listItem)
{
    GrpmPackageListItem *newItem;

    if (listItem->pkg->doWhat != GRPM_PACKAGE_DOWHAT_NOTHING) {
	/* Already on the list */
	return;
    }

    newItem = grpmPackageListAddPackage(action->list, listItem->pkg);
    newItem->flags = listItem->flags;
    newItem->pkg->doWhat = GRPM_PACKAGE_DOWHAT_INSTALL;
}

void grpmActionAddUninstallPackage(GrpmActionList *action,
				   GrpmPackageListItem *listItem)
{
    GrpmPackageListItem *newItem;

    if (listItem->pkg->doWhat != GRPM_PACKAGE_DOWHAT_NOTHING) {
	/* Already on the list */
	return;
    }
    
    newItem = grpmPackageListAddPackage(action->list, listItem->pkg);
    newItem->flags = listItem->flags;
    newItem->pkg->doWhat = GRPM_PACKAGE_DOWHAT_UNINSTALL;
}

void grpmActionRemovePackage(GrpmActionList *action,
			     GrpmPackageListItem *listItem)
{
    grpmPackageListRemovePackage(action->list, listItem->pkg);
    
    listItem->pkg->doWhat = GRPM_PACKAGE_DOWHAT_NOTHING;
}

void grpmActionRemovePackagePkg(GrpmActionList *action, GrpmPackage *pkg)
{
    grpmPackageListRemovePackage(action->list, pkg);
    
    pkg->doWhat = GRPM_PACKAGE_DOWHAT_NOTHING;
}

void grpmActionInstall(GrpmActionList *action)
{
    struct rpmDependencyConflict * conflicts;
    int numConflicts;

    /* Install entry point */

    grpmActionGetConflicts(action, &installDeps, &conflicts, &numConflicts);
    rpmdepFreeConflicts(conflicts, numConflicts);
    
    if (numConflicts) {
	/* Warn user */
	gnome_ok_cancel_dialog_modal("Conflicts found.\n\n"
				     "Continue with install?",
				     (GnomeReplyCallback)grpmActionInstallCB,
				     action);
	return;
    }

    grpmActionDoInstall(action);
}

static void grpmActionInstallCB(gint reply, GrpmActionList *action)
{
    if (reply == GNOME_CANCEL) {
	rpmdepDone(installDeps);
	return;
    }

    grpmActionDoInstall(action);
}

void grpmActionCheckDepends(GrpmActionList *action)
{
    rpmDependencies rpmdep;
    struct rpmDependencyConflict * conflicts;
    int numConflicts;

    if (grpmActionGetConflicts(action, &rpmdep, &conflicts, &numConflicts)) {
	return;
    }
	
    if (numConflicts) {
	grpmActionResolveDeps(action, rpmdep, conflicts, numConflicts);
    } else {
	rpmdepDone(rpmdep);
    }
}

static gint grpmActionGetConflicts(GrpmActionList *action,
				   rpmDependencies *rpmdep,
				   struct rpmDependencyConflict **conflicts,
				   int *numConflicts)
{
    rpmdb db;
    GList *list, *plist;
    GrpmPackageListItem *pkgItem;
    GrpmPanel *panel;
    
    /* Do the scan, marking packages */
    if (rpmdbOpen("", &db, O_RDONLY, 0644)) {
	g_warning("Unable to open RPM database");
	gnome_error_dialog("Unable to open RPM database");
	return 1;
    }

    *rpmdep = rpmdepDependencies(db);
    list = action->list->packages;
    while (list) {
	pkgItem = (GrpmPackageListItem *)list->data;
	if (pkgItem->pkg->doWhat == GRPM_PACKAGE_DOWHAT_UNINSTALL) {
	    rpmdepRemovePackage(*rpmdep, pkgItem->pkg->offset);
	} else {
	    rpmdepUpgradePackage(*rpmdep, pkgItem->pkg->header, pkgItem->pkg);
	}

	list = list->next;
    }

    plist = panelList;
    while (plist) {
	panel = (GrpmPanel *)plist->data;
	if (panel->repo) {
	    list = panel->list->packages;
	    while (list) {
		pkgItem = (GrpmPackageListItem *)list->data;
		rpmdepAvailablePackage(*rpmdep, pkgItem->pkg->header,
				       pkgItem->pkg);

		list = list->next;
	    }
	}

	plist = plist->next;
    }

    /* According to the code, this can fail, but it's not  */
    /* clear under what conditions.  I'll want for someone */
    /* to see it and tell me.                              */
    if (rpmdepCheck(*rpmdep, conflicts, numConflicts)) {
	g_error("rpmdepCheck() failed! this should not happen");
    }

    rpmdbClose(db);

    return 0;
}

static void grpmActionResolveDeps(GrpmActionList *action,
				  rpmDependencies rpmdep,
				  struct rpmDependencyConflict *conflicts,
				  int numConflicts)
{
    GtkWidget *dialog, *label, *vbox, *button, *list, *scr_win;
    static char *titles[] = { "Package", "Requirement", "Suggestion",
			      "From Repo" };
    static char *widths [] = { "Maximum Package Name with info", 	
			       "Maximum Package Name >- 0.1.2.3",
			       "Maximum Package Name with info",
			       "Maximum Repo Name" };
    char *text[4];
    int i, width, row;
    int totalwidth = 0;
    int numSuggestions = 0;
    GrpmPackage *suggestion;
    
    dialog = gnome_dialog_new("Dependecies and Conflicts",
			      GNOME_STOCK_BUTTON_OK,
			      GNOME_STOCK_BUTTON_CANCEL,
			      NULL);
    gtk_window_set_policy(GTK_WINDOW(dialog), TRUE, TRUE, FALSE);
    
    gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
    gnome_dialog_set_close(GNOME_DIALOG(dialog), TRUE);

    vbox = GNOME_DIALOG(dialog)->vbox;

    label = gtk_label_new("The following problems were noticed:");
    gtk_widget_show(label);
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 2);

    /* Clist */
    
    gtk_widget_push_visual(gdk_imlib_get_visual());
    gtk_widget_push_colormap(gdk_imlib_get_colormap());
    list = gtk_clist_new_with_titles(4, titles);
    gtk_widget_pop_colormap();
    gtk_widget_pop_visual();

    gtk_clist_set_selection_mode(GTK_CLIST(list),
				 GTK_SELECTION_MULTIPLE);

    for (i = 0; i < 4; i++) {
	gtk_clist_column_title_passive (GTK_CLIST(list), i);
	width = gdk_string_width(list->style->font, widths[i]);
	totalwidth += width;
	gtk_clist_set_column_width(GTK_CLIST(list), i, width);
    }
    gtk_widget_set_usize(GTK_WIDGET(list), totalwidth + 60, 150);
    gtk_widget_show(list);

    gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0,
				(GtkSignalFunc)grpmActionFinishCheckDepsCB,
				list);

    /* put that clist in a scrolled window */

    scr_win = gtk_scrolled_window_new (NULL, NULL);
    gtk_container_add (GTK_CONTAINER(scr_win),
                       GTK_WIDGET(list));
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(scr_win),
                                    GTK_POLICY_AUTOMATIC,
                                    GTK_POLICY_ALWAYS);

    gtk_widget_show(scr_win);

    gtk_box_pack_start(GTK_BOX(vbox), scr_win, FALSE, FALSE, 2);

    /* Remove button */

    button = gtk_button_new_with_label("Ignore selected suggestions");
    gtk_widget_show(button);
    gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 2);
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
		       GTK_SIGNAL_FUNC(grpmActionResolveDepsRemoveCB), list);
    
    /* End of UI */

    for (i = 0; i < numConflicts; i++) {
	text[0] = headerSprintf(conflicts[i].byHeader,
				"%{NAME}-%{VERSION}-%{RELEASE}",
				rpmTagTable, rpmHeaderFormats, NULL);
	width = strlen(conflicts[i].needsName) + 20;
	if (conflicts[i].needsFlags & 
	    (RPMSENSE_LESS | RPMSENSE_EQUAL | RPMSENSE_GREATER))
	    width += strlen(conflicts[i].needsVersion);
	text[1] = malloc(width);
	
	strcpy(text[1], conflicts[i].needsName);
	strcat(text[1], " ");
	if (conflicts[i].needsFlags & RPMSENSE_LESS)
	    strcat(text[1], "<");
	if (conflicts[i].needsFlags & RPMSENSE_EQUAL)
	    strcat(text[1], "=");
	if (conflicts[i].needsFlags & RPMSENSE_GREATER)
	    strcat(text[1], ">");
	if (conflicts[i].needsFlags & 
	    (RPMSENSE_LESS | RPMSENSE_EQUAL | RPMSENSE_GREATER)) {
	    strcat(text[1], " ");
	    strcat(text[1], conflicts[i].needsVersion);
	}
	
	if (conflicts[i].suggestedPackage) {
	    numSuggestions++;
	    suggestion = conflicts[i].suggestedPackage;
	    text[2] = headerSprintf(suggestion->header, 
				    "%{NAME}-%{VERSION}-%{RELEASE}",
				    rpmTagTable, rpmHeaderFormats, NULL);
	    text[3] = suggestion->repo->panel->name;
	} else {
	    suggestion = NULL;
	    text[2] = NULL;
	    text[3] = NULL;
	}
	
	row = gtk_clist_append(GTK_CLIST(list), text);
	gtk_clist_set_row_data(GTK_CLIST(list), row, suggestion);
	
	free(text[0]);
	free(text[1]);
	if (text[2]) free(text[2]);
    }

    rpmdepFreeConflicts(conflicts, numConflicts);
    rpmdepDone(rpmdep);

    gtk_widget_show(dialog);
}

static void grpmActionResolveDepsRemoveCB(GtkWidget *button, GtkCList *clist)
{
    GList *selection;

    gtk_clist_freeze(clist);
    selection = g_list_last(clist->selection);
    while (selection) {
	gtk_clist_remove(clist, GPOINTER_TO_INT (selection->data));
	selection = selection->prev;
    }
    gtk_clist_thaw(clist);
}
    
static void grpmActionFinishCheckDepsCB(GtkWidget *button, GtkCList *clist)
{
    GrpmPackage *suggestion;
    GrpmPackageListItem *listItem;
    gint row;
    
    /* Add items in clist to action list */

    for (row = 0; row < clist->rows; row++) {
	suggestion = gtk_clist_get_row_data(clist, row);

	if (suggestion) {
	    listItem = grpmPackageListEntryFindByRef(suggestion->repo->list,
						     suggestion->ref);
	
	    grpmActionAddInstallPackage(actionPanel->actionList, listItem);
	}
    }

    grpmPanelRedraw(actionPanel);
}

static GtkWidget *wholeProgress;
static GtkWidget *pkgProgress;
static unsigned long wholePart;       /* The amount we just added        */
static unsigned long wholeSize;       /* The total size of all pkgs      */
static unsigned long runningWhole;    /* The total size so far installed */

static void grpmActionDoInstall(GrpmActionList *action)
{
    GtkWidget *window;
    GtkWidget *vbox, *hbox;
    GtkWidget *label;
    GtkWidget *name;
    GtkWidget *counter;
    GtkTable *table;
    GtkWidget *tableLabel;
    GtkWidget *separator;
    GtkWidget *cancel;

    GrpmPackage **packages, **newList, **oldList, **newListP, **oldListP;
    GrpmRepo *repo;
    GrpmPackage *pkg;
    GList *list, *alreadyTested;
    rpmdb db;
    gint res, *size, numPackages, numSoFar;
    gint cancelled = 0;
    gchar *what;
    gchar buf[BUFSIZ];

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Install/Uninstall Progress");
    gtk_widget_set_usize (window, 500, 200);
    gtk_widget_show(window);

    vbox = gtk_vbox_new(FALSE, 5);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);
    gtk_container_add(GTK_CONTAINER(window), vbox);
    gtk_widget_show(vbox);

    hbox = gtk_hbox_new(FALSE, 5);
    gtk_container_set_border_width(GTK_CONTAINER(hbox), 5);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);
    gtk_widget_show(hbox);

    label = gtk_label_new("Installing:");
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
    gtk_widget_show(label);

    name = gtk_label_new("");
    gtk_box_pack_start(GTK_BOX(hbox), name, FALSE, FALSE, 0);
    gtk_widget_show(name);

    counter = gtk_label_new("");
    gtk_box_pack_start(GTK_BOX(hbox), counter, FALSE, FALSE, 10);
    gtk_widget_show(counter);
    
    /* Need 2 progress bars */
    table = GTK_TABLE(gtk_table_new(0, 0, FALSE));
    gtk_table_set_homogeneous(table, 0);
    gtk_container_set_border_width(GTK_CONTAINER(table), GNOME_PAD);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(table),
		       TRUE, TRUE, GNOME_PAD_SMALL);

    tableLabel = gtk_label_new("This package:");
    gtk_misc_set_alignment(GTK_MISC(tableLabel), 1.0, 0.5);
    gtk_table_attach(table, tableLabel, 0, 1, 0, 1,
		     GTK_FILL, GTK_FILL, 5, 0);
    pkgProgress = gtk_progress_bar_new();
    gtk_table_attach(table, pkgProgress, 1, 2, 0, 1,
		     GTK_FILL, GTK_FILL, 5, 0);

    tableLabel = gtk_label_new("Total:");
    gtk_misc_set_alignment(GTK_MISC(tableLabel), 1.0, 0.5);
    gtk_table_attach(table, tableLabel, 0, 1, 1, 2,
		     GTK_FILL, GTK_FILL, 5, 0);
    wholeProgress = gtk_progress_bar_new();
    gtk_table_attach(table, wholeProgress, 1, 2, 1, 2,
		     GTK_FILL, GTK_FILL, 5, 0);

    gtk_widget_show_all(GTK_WIDGET(table));
    
    /* Separator */
    separator = gtk_hseparator_new();
    gtk_box_pack_start(GTK_BOX(vbox), separator, FALSE, TRUE, GNOME_PAD_SMALL);
    gtk_widget_show(separator);

    /* Cancel button */
    cancel = gnome_stock_or_ordinary_button(GNOME_STOCK_BUTTON_CANCEL);
    gtk_box_pack_start(GTK_BOX(vbox), cancel, FALSE, FALSE, GNOME_PAD_SMALL);
    gtk_widget_show(cancel);
    gtk_signal_connect(GTK_OBJECT(cancel), "clicked",
		       GTK_SIGNAL_FUNC(grpmActionDoInstallCancelCB),
		       &cancelled);

    /* Set modal */
    gtk_grab_add(window);
    
    /* Order the list */

    /* rpmdepOrder doesn't seem to like stuff marked for uninstall */
    rpmdepOrder(installDeps, (void ***) &packages);
    rpmdepDone(installDeps);
    /* Now copy array to larger one that can hold uninstalls as well */
    numPackages = g_list_length(action->list->packages);
    newList = newListP = malloc((numPackages + 1) * sizeof(GrpmPackage *));
    oldList = oldListP = packages;
    while (*oldListP) {
	*newListP++ = *oldListP++;
    }
    packages = newList;
    free(oldList);

    /* Copy in the uninstalls */
    list = action->list->packages;
    while (list) {
	pkg = ((GrpmPackageListItem *)list->data)->pkg;
	if (pkg->doWhat == GRPM_PACKAGE_DOWHAT_UNINSTALL) {
	    *newListP++ = pkg;
	}
	list = list->next;
    }
    *newListP = NULL;

    gtk_widget_realize(window);
    GTK_FLUSH;

    /* Compute total size */
    wholeSize = 0;
    newListP = packages;
    while (*newListP) {
	res = headerGetEntry((*newListP)->header, RPMTAG_ARCHIVESIZE, NULL,
		       (void **)&size, NULL);
	wholeSize += *size;
	newListP++;
    }
    runningWhole = 0;

    /* We do a prelim pass through the packages looking for     */
    /* removable repos that are not available.  We catch them   */
    /* now rather than in the middle of the install when the    */
    /* dependencies could get all screwed up.  It could still   */
    /* happen with FTP and HTTP repos, but we can catch LOCALs. */
    res = 0;
    alreadyTested = NULL;
    while (*packages) {
	pkg = *packages;
	if (pkg->doWhat != GRPM_PACKAGE_DOWHAT_UNINSTALL) {
	    repo = pkg->repo;
	    if (repo->isRemovable) {
		if (! g_list_find(alreadyTested, repo)) {
		    if (grpmRepoOpenSession(repo)) {
			res = 1;
		    }
		    grpmRepoCloseSession(repo);
		    alreadyTested = g_list_append(alreadyTested, repo);
		}
	    }
	}
	packages++;
    }
    g_list_free(alreadyTested);
    if (res) {
	/* Error dialogs are already showing */
	gtk_widget_destroy(window);
	free(newList);
	return;
    }
    packages = newList;
    
    /* The meat of it */
    if (rpmdbOpen("", &db, O_RDWR | O_CREAT, 0644)) {
	gtk_widget_destroy(window);
	g_warning("Unable to open RPM database");
	gnome_error_dialog("Unable to open RPM database");
	free(newList);
	return;
    }
    
    numSoFar = 0;
    while (*packages && !cancelled) {
	numSoFar++;
	g_snprintf(buf, sizeof(buf), "(%d of %d)", numSoFar, numPackages);
	gtk_label_set_text(GTK_LABEL(counter), buf);
	
	pkg = *packages;
	if (pkg->doWhat == GRPM_PACKAGE_DOWHAT_UNINSTALL) {
	    res = grpmActionUninstallAux(action, &db,
					 GTK_LABEL(label), GTK_LABEL(name),
					 pkg);
	    what = "uninstalling";
	} else {
	    res = grpmActionInstallAux(action, &db,
				       GTK_LABEL(label), GTK_LABEL(name),
				       pkg);
	    what = "installing";
	}

	if (!res) {
	    /* Remove from action list */
	    grpmActionRemovePackagePkg(action, pkg);
	} else {
	    /* Display error */
	    printf("Error installing/uninstalling package: %d\n", res);
	    g_snprintf(buf, sizeof(buf),
		       "Error %s package: %s", what, pkg->name);
	    g_warning(buf);
	    gnome_error_dialog(buf);
	}

	if (pkg->doWhat == GRPM_PACKAGE_DOWHAT_UNINSTALL) {
	    /* Uninstalls don't update progress meters as they go */
	    headerGetEntry(pkg->header, RPMTAG_ARCHIVESIZE, NULL,
			   (void **)&size, NULL);
	    runningWhole += *size;
	    gtk_progress_bar_update(GTK_PROGRESS_BAR(wholeProgress),
				    1.0 * runningWhole / wholeSize);
	}

	GTK_FLUSH;
	packages++;
    }

    free(newList);
    rpmdbClose(db);

    if (cancelled) {
	gnome_ok_dialog("Cancelled");
    }

    /* Need to rescan installed panel */
    grpmPanelReload(installedPanel);

    /* Re-set states in other panels (does redraw) */
    grpmPanelResetStates();

    grpmPanelRedraw(actionPanel);
    
    /* Destroy the dialog */
    gtk_widget_destroy(window);
}

static void grpmActionDoInstallCancelCB(GtkWidget *button, gint *cancelled)
{
    *cancelled = 1;
}

static gint grpmActionUninstallAux(GrpmActionList *action,
				   rpmdb *db,
				   GtkLabel *label, GtkLabel *name,
				   GrpmPackage *pkg)
{
    gint res = 0;
    
    GTK_FLUSH;
    gtk_label_set_text(label, "Uninstalling:");
    gtk_label_set_text(name, pkg->name);
    GTK_FLUSH;

#ifdef TESTING    
    printf("removing: %s\n", pkg->name);
    sleep(1);
#else    
    res = rpmRemovePackage("", *db, pkg->offset, 0);
#endif    
    
    return res;
}

static gint grpmActionInstallAux(GrpmActionList *action,
				 rpmdb *db,
				 GtkLabel *label, GtkLabel *name,
				 GrpmPackage *pkg)
{
    gchar installFile[BUFSIZ];
    gint rmFile = 0;
    gint res = 0;
    int fd, flags;
    
    gtk_label_set_text(name, pkg->name);
    gtk_progress_bar_update(GTK_PROGRESS_BAR(pkgProgress), 0.0);
    wholePart = 0;

    /* Need to check for downloading */
    switch (pkg->repo->type) {
      case GRPM_REPO_LOCAL:
	break;
      case GRPM_REPO_FTP:
	gtk_label_set_text(label, "Downloading (FTP):");
	rmFile = 1;
	break;
      case GRPM_REPO_HTTP:
	gtk_label_set_text(label, "Downloading (HTTP):");
	rmFile = 1;
	break;
    }
    GTK_FLUSH;

    if (grpmRepoGetPackageFileName(pkg->repo, pkg, installFile)) {
	return 1;
    }
	
    gtk_label_set_text(label, "Installing:");
    GTK_FLUSH;

#ifdef TESTING
    printf("Installing: %s, %s\n", pkg->name, installFile);
    sleep(1);
#else    
    if ((fd = open(installFile, O_RDONLY)) < 0) {
	if (rmFile) {
	    unlink(installFile);
	}
	return 1;
    }

    /* When we allow installs as well as upgrades this will mean something */
    flags = RPMINSTALL_UPGRADE;
    res = rpmInstallPackage("", *db, fd, NULL, flags,
			    grpmActionUpdatePkgProgress, NULL);
#endif

    if (rmFile) {
	unlink(installFile);
    }
    
    return res;
}

/* If we could pass data to the rpmNotifyFunction we could get */
/* away without having globals laying around.                  */
static void grpmActionUpdatePkgProgress(const unsigned long amount,
					const unsigned long total)
{
    gfloat p;

    p = 1.0 * amount / total;
    gtk_progress_bar_update(GTK_PROGRESS_BAR(pkgProgress), p);

    runningWhole = runningWhole - wholePart + amount;
    wholePart = amount;

    p = 1.0 * runningWhole / wholeSize;
    gtk_progress_bar_update(GTK_PROGRESS_BAR(wholeProgress), p);

    GTK_FLUSH;
}
