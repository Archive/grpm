#include "grpm.h"

GrpmCollection *grpmCollectionNew(gchar *name)
{
    GrpmCollection *res;

    res = g_new(GrpmCollection, 1);

    res->list = grpmPackageListNew();

    res->filters = NULL;   /* May not ever be used */
    res->name = g_strdup(name);

    return res;
}

void grpmCollectionDestroy(GrpmCollection *collection)
{
    grpmPackageListFree(collection->list, FALSE);
    g_free(collection->name);

    g_free(collection);
}

void grpmCollectionAddPackage(GrpmCollection *collection,
			      GrpmPackageListItem *listItem)
{
    GrpmPackageListItem *newItem;

    newItem = grpmPackageListAddPackage(collection->list, listItem->pkg);
    newItem->flags = listItem->flags;
}

void grpmCollectionRemovePackage(GrpmCollection *collection,
				 GrpmPackageListItem *listItem)
{
    grpmPackageListRemovePackage(collection->list, listItem->pkg);
}
