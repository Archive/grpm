#include "grpm.h"

GdkColor unknownColor = {0, 200<<8, 200<<8, 200<<8};
GdkColor installedColor = {0, 250<<8, 200<<8, 200<<8};
GdkColor uninstalledColor = {0, 150<<8, 250<<8, 150<<8};
GdkColor oldpackageColor = {0, 200<<8, 200<<8, 250<<8};
GdkColor newpackageColor = {0, 0, 255<<8, 0};

static void grpmColorLegendOKCB(void *w, void *d);
    
static GtkWidget *dialog = NULL;

void grpmColorInit(void)
{
    static int done = 0;

    if (done) {
	return;
    }

    gdk_imlib_best_color_get(&unknownColor);
    gdk_imlib_best_color_get(&installedColor);
    gdk_imlib_best_color_get(&uninstalledColor);
    gdk_imlib_best_color_get(&oldpackageColor);
    gdk_imlib_best_color_get(&newpackageColor);
    
    done = 1;
}

void grpmColorLegend(void)
{
    GtkBox *vbox;
    GtkWidget *label, *hbox;

    return;
    
    if (dialog) {
	/* XXX really should de-iconify */
	gdk_window_raise(dialog->window);
	return;
    }

    dialog = gnome_dialog_new("Color Legend",
			      GNOME_STOCK_BUTTON_OK,
			      NULL);
    gtk_window_set_policy(GTK_WINDOW(dialog), TRUE, TRUE, FALSE);
    gnome_dialog_set_close(GNOME_DIALOG(dialog), TRUE);

    vbox = GTK_BOX(GNOME_DIALOG(dialog)->vbox);

    hbox = gtk_hbox_new(FALSE, 5);
    gdk_window_set_background(hbox->window, &newpackageColor);
    label = gtk_label_new("Upgradable packages");
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 2);
    gtk_box_pack_start(vbox, hbox, FALSE, FALSE, 2);
    gtk_widget_show_all(hbox);

    gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0,
				(GtkSignalFunc)grpmColorLegendOKCB, NULL);
    
    gtk_widget_show(dialog);
}

static void grpmColorLegendOKCB(void *w, void *d)
{
    dialog = NULL;
}
