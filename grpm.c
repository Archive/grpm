#include <string.h>
#include <gnome.h>

#include "grpm.h"

#define PACKAGE "grpm"

void setStatusBar(gchar *s);
void messageHandler(gchar *s);
void warningHandler(gchar *s);
void errorHandler(gchar *s);
void setErrorHandlers(void);

GtkWidget *mainWindowNew(void);
void quitProgramCB(void);
void reallyQuitCB(gint reply, void *data);
void reallyQuit(void);
void newRepositoryCB(void);
GrpmPanel *newRepository(gchar *ref, gchar *name, gchar *cache,
			 gint sortBy, gboolean viewGroups,
			 gchar *currentGroup,
			 gboolean inNotebook, gboolean isRemovable,
			 gint lastScanned);
gint grpmMakeCollectionPanel(GrpmCollection *collection);
void queryCB(void);
void configureGrpmCB(void);
void mainWindowAbout(gboolean modal);
void mainWindowAboutCB(void);
void scanCB(void);
void reloadCB(void);
void colorLegendCB(void);
void removePanelCB(GrpmPanel *panel);
GrpmPanel *newPanel(gchar *ref, gchar *name);
void saveRepositoryConfigs(void);
void readGrpmConfigs(void);

void newRepositoryOK(GtkWidget *w, void *data);

gint createActionAndInstalledPanel(void);

GnomeUIInfo filemenu[] = {
    GNOMEUIINFO_ITEM_STOCK("_New Repository", "New Repository",
			   newRepositoryCB, GNOME_STOCK_MENU_NEW),
    GNOMEUIINFO_ITEM_STOCK("_Search Repositories", "Search",
			   queryCB, GNOME_STOCK_MENU_NEW),
/*    GNOMEUIINFO_ITEM("Configure", "Configure", configureGrpmCB, NULL), */
    GNOMEUIINFO_ITEM_STOCK("E_xit", "Exit program",
			   quitProgramCB, GNOME_STOCK_MENU_EXIT),
    GNOMEUIINFO_END
};

GnomeUIInfo refreshmenu[] = {
    GNOMEUIINFO_ITEM_STOCK("_Scan all", "Scan all repos for new packages",
			   scanCB, GNOME_STOCK_PIXMAP_REFRESH),
    GNOMEUIINFO_ITEM_STOCK("_Reload all", "Reload all repos",
			   reloadCB, GNOME_STOCK_PIXMAP_REFRESH),
    GNOMEUIINFO_END
};

GnomeUIInfo helpmenu[] = {
/*    GNOMEUIINFO_ITEM("Color legend", "Meanings of the colors",
      colorLegendCB, NULL),*/
    GNOMEUIINFO_ITEM_STOCK("_About...", "Info about this program",
			   mainWindowAboutCB, GNOME_STOCK_MENU_ABOUT),
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_HELP("grpm"),
    GNOMEUIINFO_END
};
 
GnomeUIInfo mainmenu[] = {
    GNOMEUIINFO_SUBTREE("_File", filemenu),
    GNOMEUIINFO_SUBTREE("_Refresh", refreshmenu),
    GNOMEUIINFO_SUBTREE("_Help", helpmenu),
    GNOMEUIINFO_END
};

GtkWidget *mainWindow;
GtkWidget *notebook;
GtkWidget *statusBar;

gint verbosity = 2;

int main(int argc, char *argv[])
{
    /* Misc GNOME initialization stuff */
    
    bindtextdomain (PACKAGE, GNOMELOCALEDIR);
    textdomain (PACKAGE);
    gnome_init(GRPM_NAME, GRPM_VERSION, argc, argv);

    /* Now to the real stuff */
    
    rpmReadConfigFiles(NULL, NULL, NULL, 0);

    gtk_widget_push_visual(gdk_imlib_get_visual());
    gtk_widget_push_colormap(gdk_imlib_get_colormap());

    mainWindow = mainWindowNew();
    setErrorHandlers();
    gtk_widget_show(GTK_WIDGET(mainWindow));

    /* Create the installed and action panels */
    createActionAndInstalledPanel();
    
    /* Create all existing panels */
    readGrpmConfigs();

#ifdef TELL_ME_WHO_WROTE_THIS_PROGRAM
    mainWindowAbout(TRUE);
#endif
    
    gtk_main();

    saveRepositoryConfigs();
	
    return 0;
}

void quitProgramCB(void)
{
    /* No need to save config data because we write it as it is changed */
    
    if (! actionPanel->actionList->list->packages) {
	reallyQuit();
	return;
    }

    gnome_question_dialog_modal("Packages are still listed in "
				"the action panel.\n\n"
				"Are you sure you want to quit?",
				(GnomeReplyCallback)reallyQuitCB, NULL);
}

void reallyQuitCB(gint reply, void *data)
{
    if (reply == GNOME_YES) {
	reallyQuit();
    }
}

void reallyQuit(void)
{
    gtk_main_quit();
}

void colorLegendCB(void)
{
    grpmColorLegend();
}

void scanCB(void)
{
    g_list_foreach(panelList, (GFunc) grpmPanelScan, NULL);
}

void reloadCB(void)
{
    g_list_foreach(panelList, (GFunc) grpmPanelReload, NULL);
}

GtkWidget *mainWindowNew(void)
{
    GtkWidget *app;
    GtkWidget *vbox;

    app = gnome_app_new("grpm", "Gnome RPM Manager");
    gtk_widget_realize(app);

    gtk_signal_connect(GTK_OBJECT(app), "delete_event",
		       GTK_SIGNAL_FUNC(quitProgramCB), NULL);

    /* Create menu and toolbar */
    gnome_app_create_menus(GNOME_APP(app), mainmenu);
    /*gnome_app_create_toolbar(GNOME_APP(app), toolbar);*/

    /* main vbox */
    vbox = gtk_vbox_new(FALSE, 0);
    gtk_container_set_border_width (GTK_CONTAINER (vbox), 0);
    gtk_widget_show(vbox);

    /* notebook */
    notebook = gtk_notebook_new();
    gtk_notebook_set_tab_pos(GTK_NOTEBOOK(notebook), GTK_POS_TOP);
    gtk_notebook_set_scrollable (GTK_NOTEBOOK(notebook), TRUE);
    gtk_widget_set_usize(notebook, 600, 280);
    gtk_container_set_border_width(GTK_CONTAINER(notebook), 5);
    gtk_box_pack_start(GTK_BOX(vbox), notebook, TRUE, TRUE, 0);
    gtk_widget_show(notebook);
    
    /* add a status bar */
    statusBar = gtk_statusbar_new();
    gtk_box_pack_start(GTK_BOX(vbox), statusBar, FALSE, FALSE, 0);
    gtk_widget_show(statusBar);
    
    gnome_app_set_contents(GNOME_APP(app), vbox);
    
    return app;
}

gint createInstalledPanel(void)
{
    GrpmRpmdb *rdb;
    GrpmPanel *panel;
    GtkWidget *label;
    gchar *name = "Installed";
    SortTypes sortBy;
    gboolean viewGroups;
    gchar *currentGroup;

    sortBy = gnome_config_get_int("/grpm/Installed/sortBy=1");
    viewGroups = gnome_config_get_bool("/grpm/Installed/viewGroups=True");
    currentGroup = gnome_config_get_string("/grpm/Installed/currentGroup");
    
    rdb = grpmRpmdbNew();
    panel = grpmPanelNewRpmdb(rdb, name, sortBy, viewGroups, currentGroup,
			      actionPanel, NULL);

    label = gtk_label_new(name);
    gtk_widget_show(label);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), panel->widget, label);

    panelList = g_list_append(panelList, panel);
    installedPanel = panel;

    return 0;
}

gint createActionAndInstalledPanel(void)
{
    GrpmActionList *action;
    GrpmPanel *panel;
    GtkWidget *label;
    gchar *name = "Action";
    SortTypes sortBy;
    gboolean viewGroups;
    gchar *currentGroup;

    sortBy = gnome_config_get_int("/grpm/Action/sortBy=1");
    viewGroups = gnome_config_get_bool("/grpm/Action/viewGroups=True");
    currentGroup = gnome_config_get_string("/grpm/Action/currentGroup");
    
    action = grpmActionNew();
    panel = grpmPanelNewAction(action, name, sortBy, viewGroups, currentGroup,
			       NULL);
    actionPanel = panel;

    createInstalledPanel();

    label = gtk_label_new(name);
    gtk_widget_show(label);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), panel->widget, label);

    panelList = g_list_append(panelList, panel);

    return 0;
}

gint grpmMakeCollectionPanel(GrpmCollection *collection)
{
    GrpmPanel *panel;
    GtkWidget *label;
    
    panel = grpmPanelNewCollection(collection, collection->name, actionPanel,
				   removePanelCB);

    label = gtk_label_new(collection->name);
    gtk_widget_show(label);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), panel->widget, label);

    panelList = g_list_append(panelList, panel);

    return 0;
}

void queryCB(void)
{
    grpmSearchWindowNew(grpmMakeCollectionPanel);
}

void removePanelCB(GrpmPanel *panel)
{
    panelList = g_list_remove(panelList, panel);
    saveRepositoryConfigs();
}

/* XXX - This stuff belongs somewhere else!  It really should be
   generalized to the configure panel for any GrpmPanel */

#define CONFIG_INT  1
#define CONFIG_TEXT 2
#define CONFIG_BOOL 3

struct _config_entry {
    gchar *name;
    gint type;
    gpointer var;
    GtkWidget *entry;
};

gchar *newPanelRef;
gchar *newPanelName;
gboolean newPanelRemovable;

struct _config_entry configs[] = {
    { "Ref",        CONFIG_TEXT, &newPanelRef, NULL },
    { "Name",       CONFIG_TEXT, &newPanelName, NULL },
    { "Is Removable", CONFIG_BOOL, &newPanelRemovable, NULL },
    
    { NULL, 0, NULL }
};

void generateConfigWidgets(GtkWidget *parentBox, struct _config_entry *configs)
{
    GtkWidget *table, *label, *entry;
    struct _config_entry *p;
    gchar buf[BUFSIZ];
    gint rows;
    
    if (! configs)
	return;

    rows = 0;
    p = configs;
    while (p->name) {
	rows++;
	p++;
    }

    table = gtk_table_new(rows, 2, FALSE);
    gtk_widget_show(table);
    gtk_box_pack_start(GTK_BOX(parentBox), table, TRUE, TRUE, 0);

    rows = 0;
    p = configs;
    while (p->name) {
	label = gtk_label_new(p->name);
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_RIGHT);
	gtk_widget_show(label);
	
	entry = gtk_entry_new();
	gtk_widget_show(entry);
	p->entry = entry;

	if (p->type == CONFIG_INT) {
	    g_snprintf(buf, sizeof(buf), "%d", *(gint *)(p->var));
	    gtk_entry_set_text(GTK_ENTRY(entry), buf);
	} else if (p->type == CONFIG_TEXT) {
	    if (*(gchar **)(p->var)) {
		gtk_entry_set_text(GTK_ENTRY(entry), *(gchar **)(p->var));
	    }
	} else if (p->type == CONFIG_BOOL) {
	    gtk_widget_destroy(entry);
	    entry = gtk_check_button_new_with_label(p->name);
	    gtk_widget_show(entry);
	    p->entry = entry;
	    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(entry),
					*(gboolean *)p->var);
	}

	gtk_table_attach(GTK_TABLE(table), label, 0, 1, rows, rows + 1,
			 GTK_FILL, GTK_FILL,
			 5, 0);
	gtk_table_attach(GTK_TABLE(table), entry, 1, 2, rows, rows + 1,
			 GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			 0, 0);

	rows++;
	p++;
    }
}

void newRepositoryCB(void)
{
    GtkWidget *window, *box;

    /* Main Window */
    window = gnome_dialog_new("Make New Repository",
			      GNOME_STOCK_BUTTON_OK,
			      GNOME_STOCK_BUTTON_CANCEL,
			      NULL);
    gtk_window_set_modal(GTK_WINDOW(window), TRUE);
    gnome_dialog_set_close(GNOME_DIALOG(window), TRUE);

    /* Vbox */
    box = GNOME_DIALOG(window)->vbox;
    
    /* Make the widgets */
    newPanelRef = NULL;
    newPanelName = NULL;
    newPanelRemovable = FALSE;
    generateConfigWidgets(box, configs);

    /* Buttons */
    gnome_dialog_button_connect(GNOME_DIALOG(window), 0,
				(GtkSignalFunc)newRepositoryOK,	NULL);

    gtk_widget_show(window);
}

void newRepositoryOK(GtkWidget *w, void *data)
{
    struct _config_entry *p = configs;
    gchar *s;
    gint x;
    GrpmPanel *panel;
    
    while (p->name) {

	if (p->type == CONFIG_INT) {
	    s = gtk_entry_get_text(GTK_ENTRY(p->entry));
	    sscanf(s, "%d", &x);
	    *(gint *)(p->var) = x;
	} else if (p->type == CONFIG_TEXT) {
	    if (*(gchar **)(p->var)) {
		g_free(*(gchar **)(p->var));
	    }
	    s = gtk_entry_get_text(GTK_ENTRY(p->entry));
	    *(gchar **)(p->var) = g_strdup(s);;
	} else if (p->type == CONFIG_BOOL) {
	    *(gboolean *)(p->var) =
		(GTK_WIDGET_STATE(p->entry) == GTK_STATE_ACTIVE);
	}
	
	p++;
    }

    panel = newRepository(newPanelRef, newPanelName, NULL,
			  SORT_NAME, TRUE, NULL, FALSE, newPanelRemovable, 0);
    grpmPanelReload(panel);
    
    g_free(newPanelRef);
    g_free(newPanelName);

    newPanelRef = NULL;
    newPanelName = NULL;
    newPanelRemovable = FALSE;
    
    saveRepositoryConfigs();
}

GrpmPanel *newRepository(gchar *ref, gchar *name, gchar *cache,
			 gint sortBy, gboolean viewGroups,
			 gchar *currentGroup,
			 gboolean inNotebook, gboolean isRemovable,
			 gint lastScanned)
{
    GrpmRepoType type;
    GrpmRepo *repo;
    GrpmPanel *panel;
    GtkWidget *label;
    
    if (!strncmp("ftp://", ref, 6)) {
	type = GRPM_REPO_FTP;
    } else if (!strncmp("http://", ref, 7)) {
	type = GRPM_REPO_HTTP;
    } else {
	type = GRPM_REPO_LOCAL;
    }

    if (sortBy <= SORT_FIRST || sortBy >= SORT_LAST) {
	sortBy = SORT_NAME;
    }
    
    repo = grpmRepoNew(type, ref, cache, installedPanel->rdb,
		       isRemovable, name);
    panel = grpmPanelNewRepo(repo, name, sortBy, viewGroups,
			     currentGroup, actionPanel, removePanelCB);

    label = gtk_label_new(name);
    gtk_widget_show(label);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), panel->widget, label);

    panelList = g_list_append(panelList, panel);
    
    return panel;
}

void configureGrpmCB(void)
{
    /* Do any configuration, and save it */
}

void readGrpmConfigs(void)
{
    gchar buf[BUFSIZ];
    gint count, x;
    gchar *ref, *name, *cacheFile, *currentGroup;
    gint sortBy, lastScanned;
    gboolean viewGroups, inNotebook, isRemovable;

    count = gnome_config_get_int("/grpm/Misc/repoCount=0");
    if (count <= 0) {
	return;
    }

    for (x = 1; x <= count; x++) {
	g_snprintf(buf, sizeof(buf), "/grpm/Repo%d/", x);
	gnome_config_push_prefix(buf);
	
	ref = gnome_config_get_string("ref");
	name = gnome_config_get_string("name");
	cacheFile = gnome_config_get_string("cacheFile");
	sortBy = gnome_config_get_int("sortBy=1");
	viewGroups = gnome_config_get_bool("viewGroups=True");
	currentGroup = gnome_config_get_string("currentGroup");
	inNotebook = gnome_config_get_bool("inNotebook=True");
	isRemovable = gnome_config_get_bool("isRemovable=False");
	lastScanned = gnome_config_get_int("lastScanned=0");

	gnome_config_pop_prefix();

	newRepository(ref, name, cacheFile, sortBy, viewGroups,
		      currentGroup, inNotebook, isRemovable, lastScanned);
	g_free(ref);
	g_free(name);
	g_free(cacheFile);
    }
}

void saveRepositoryConfigs(void)
{
    gchar buf[BUFSIZ];
    gint count, x;
    GList *p;
    GrpmPanel *panel;

    /* Clear all the existing entries */
    count = gnome_config_get_int("/grpm/Misc/repoCount=0");
    if (count > 0) {
	for (x = 1; x <= count; x++) {
	    g_snprintf(buf, sizeof(buf), "/grpm/Repo%d", x);
	    gnome_config_clean_section(buf);
	}
    }

    /* Write out the data */
    p = panelList;
    count = 0;
    while (p) {
	panel = (GrpmPanel *)p->data;

	if (panel->rdb || panel->actionList) {
	    if (panel->rdb) {
		gnome_config_push_prefix("/grpm/Installed/");
	    } else {
		gnome_config_push_prefix("/grpm/Action/");
	    }
	    gnome_config_set_int("sortBy", panel->sortBy);
	    gnome_config_set_bool("viewGroups", panel->viewGroups);
	    gnome_config_set_string("currentGroup", panel->currentGroup ?
				    panel->currentGroup : "");
	    gnome_config_pop_prefix();
	}
	
	if (! panel->repo) {
	    p = p->next;
	    continue;
	}
	
	count++;

	g_snprintf(buf, sizeof(buf), "/grpm/Repo%d/", count);
	gnome_config_push_prefix(buf);

	gnome_config_set_string("ref", panel->repo->ref);
	gnome_config_set_string("name", panel->name);
	gnome_config_set_string("cacheFile",
				panel->repo->cache ? panel->repo->cache : "");
	gnome_config_set_int("sortBy", panel->sortBy);
	gnome_config_set_bool("viewGroups", panel->viewGroups);
	gnome_config_set_string("currentGroup",
				panel->currentGroup ?
				panel->currentGroup : "");
	gnome_config_set_bool("inNotebook", panel->inNotebook);
	gnome_config_set_bool("isRemovable", panel->repo->isRemovable);
	gnome_config_set_int("lastScanned", panel->lastScanned);
	
	gnome_config_pop_prefix();
	
	p = p->next;
    }
    gnome_config_set_int("/grpm/Misc/repoCount", count);

    gnome_config_sync();
    
}

void mainWindowAbout(gboolean modal)
{
    GtkWidget *about;
    const gchar *authors[] = {
	"Marc Ewing",
	NULL
    };

    about = gnome_about_new("Gnome RPM Manager", GRPM_VERSION,
			    "Copyright (c) 1998 Red Hat Software, Inc.",
			    
			    authors,
			    
			    "The Gnome RPM Manager helps you locate, "
			    "install, uninstall, and upgrade RPM packages."
			    "\n\n"
			    "THIS IS AN ALPHA RELEASE.\n\n"
			    "It could corrupt your RPM database,\n"
			    "though it seems to work fine for me.\n\n"
			    "Read the README!",
			    
			    NULL);
    gtk_widget_show(about);

    if (modal) {
	gtk_grab_add(about);
    }
}

void mainWindowAboutCB(void)
{
    mainWindowAbout(FALSE);
}

/**********************************************************************/

/* Error handlers */

void setStatusBar(gchar *s)
{
    gtk_statusbar_pop(GTK_STATUSBAR(statusBar), 1);
    gtk_statusbar_push(GTK_STATUSBAR(statusBar), 1, s);

    /*gtk_widget_draw(statusBar, NULL);*/
#if 1
    while (gtk_events_pending()) {
	gtk_main_iteration();
    }
#endif    
}

void messageHandler(gchar *s)
{
    setStatusBar(s);
    if (verbosity > 2) {
	printf("M: %s\n", s);
    }
}

void errorHandler(gchar *s)
{
    if (verbosity > 0) {
	fprintf(stderr, "E: %s\n", s);
    }
}

void warningHandler(gchar *s)
{
    if (verbosity > 1) {
	printf("W: %s\n", s);
    }
}

void setErrorHandlers(void)
{
    /*g_set_error_handler((GErrorFunc) errorHandler);*/
    g_set_warning_handler((GErrorFunc) warningHandler);
    g_set_message_handler((GErrorFunc) messageHandler);
    g_set_print_handler((GErrorFunc) printf);
}
