#ifndef _GRPM_H_
#define _GRPM_H_

#include <gnome.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <rpm/rpmlib.h>
#include <rpm/header.h>

#define GRPM_VERSION "0.1"
#define GRPM_NAME "grpm"

/* A Package holds the information in an RPM package */

typedef enum {
    GRPM_PACKAGE_STATE_UNKNOWN,
    GRPM_PACKAGE_STATE_OLDPACKAGE,  /* Newer version installed. */
                                    /* For installed packages, a newer one */
                                    /* is available */
    GRPM_PACKAGE_STATE_INSTALLED,   /* This exact package is installed */
    GRPM_PACKAGE_STATE_UNINSTALLED, /* No version is installed */
    GRPM_PACKAGE_STATE_NEWPACKAGE   /* Older version is installed */
} GrpmPackageStateType;

#define GRPM_PACKAGE_DOWHAT_NOTHING    0
#define GRPM_PACKAGE_DOWHAT_INSTALL    (1 << 0)
#define GRPM_PACKAGE_DOWHAT_UNINSTALL  (1 << 1)

struct _GrpmPackage {
    Header header;
    gchar *ref;           /* relative to repo->ref   */
    gchar *name;          /* the actual package name */
    int offset;           /* If this is in the db, this is the offset */
    struct _GrpmRepo *repo;

    GrpmPackageStateType state;
    
    gint doWhat;
};

typedef struct _GrpmPackage GrpmPackage;

/**************************************************************************/

/* The PackageListItem is the GList element used in lists of GrpmPackages */

#define GRPM_PACKAGE_FLAG_MARKED  (1 << 0)

struct _GrpmPackageListItem {
    GrpmPackage *pkg;
    gint flags;
};

typedef struct _GrpmPackageListItem GrpmPackageListItem;

/**************************************************************************/

struct _GrpmPackageList {
    GList *packages;
    GHashTable *refHash;
    GHashTable *nameHash;
};

typedef struct _GrpmPackageList GrpmPackageList;

/**************************************************************************/

/* An Rpmdb represents an RPM database */

struct _GrpmRpmdb {
    GrpmPackageList *list;
    gchar *name;
};

typedef struct _GrpmRpmdb GrpmRpmdb;

/**************************************************************************/

/* A Repo represents a repository of RPMs */

typedef enum {
    GRPM_REPO_LOCAL,
    GRPM_REPO_FTP,
    GRPM_REPO_HTTP,
} GrpmRepoType;

typedef gint (*GrpmRepoOpenSessionType)(struct _GrpmRepo *repo);
typedef void (*GrpmRepoCloseSessionType)(struct _GrpmRepo *repo);
typedef gchar **(*GrpmRepoListFilesType)(struct _GrpmRepo *repo);
typedef Header (*GrpmRepoGetHeaderType)(struct _GrpmRepo *repo,
					gchar *file, gint *isSource);
typedef gint (*GrpmRepoStatFileType)(struct _GrpmRepo *repo, gchar *file);
typedef gint (*GrpmRepoGetFileToFdType)(struct _GrpmRepo *repo,
					gchar *ref, int fd);

struct _GrpmRepo {
    GrpmPackageList *list;
    gchar *name;
    gchar *ref;
    gchar *cache;
    GrpmRepoType type;

    /* Pointer to the GrpmRpmdb */
    GrpmRpmdb *installedRpmdb;

    /* The panel that "owns" this repo */
    struct _GrpmPanel *panel;
    
    GrpmRepoOpenSessionType openSession;
    GrpmRepoCloseSessionType closeSession;
    GrpmRepoListFilesType listFiles;
    GrpmRepoGetHeaderType getHeader;
    GrpmRepoStatFileType statFile;
    GrpmRepoGetFileToFdType getFileToFd;

    /* If set, this repo is on removable media (LOCAL only) */
    gboolean isRemovable;
    
    /* Session related data */
    gint sock;
    gchar *host;
    gchar *user;
    gchar *pass;
    gchar *path;
};

typedef struct _GrpmRepo GrpmRepo;

/**************************************************************************/

/* These filter types mean:
   SELECTED: what is selected
   NEW_SINCE: packages with typestamp newer than data
   FOREIGN: packages that are not in reference (in any version)
   NEW: packages that are newer versions of those in reference
   ALL: all packages
*/

typedef enum {
    GRPM_COLLECTION_FILTER_SELECTED,
    GRPM_COLLECTION_FILTER_NEW_SINCE,
    GRPM_COLLECTION_FILTER_FOREIGN,
    GRPM_COLLECTION_FILTER_NEW,
    GRPM_COLLECTION_FILTER_ALL
} GrpmFilterType;

/* A filter defines a subset of the RPMs in an GrpmList */

struct _GrpmFilter {
    GrpmPackageList *targetList;
    GrpmPackageList *referenceList;

    GrpmFilterType type;
    void *data;
};

typedef struct _GrpmFilter GrpmFilter;

/**************************************************************************/

/* A Collection defines a set of packages in terms of other
   package lists.
*/

struct _GrpmCollection {
    GrpmFilter *filters;
    GrpmPackageList *list;
    gchar *name;
};

typedef struct _GrpmCollection GrpmCollection;

/**************************************************************************/

/* An Action list is like a collection, but is not made from filters */

struct _GrpmActionList {
    GrpmPackageList *list;
};

typedef struct _GrpmActionList GrpmActionList;

/**************************************************************************/

/* A Panel is a "presentation" of a Repo or Collection */
/* It is the first and only way packages are displayed */

typedef enum {
    SORT_FIRST,   /* unused */
    SORT_NAME,
    SORT_SIZE,
    SORT_DATE,
    SORT_STATE,
    SORT_LAST,    /* unused */
} SortTypes;

typedef struct _GrpmPanel GrpmPanel;

typedef void (*GrpmPanelDestroyFunc)(GrpmPanel *panel);

struct _GrpmPanel {
    /* One of these is non-NULL */
    GrpmRepo *repo;
    GrpmCollection *collection;
    GrpmRpmdb *rdb;
    GrpmActionList *actionList;

    GrpmPanelDestroyFunc destroyFunc;
    
    /* *->list */
    GrpmPackageList *list;

    GrpmPanel *actionPanel;
    
    /* These are exclusively display-related variables */
    
    gchar *name;
    GtkWidget *widget;     /* The whole panel */
    GtkCTree *ctree;
    GtkWidget *ctree_swin;
    GtkWidget *clist;
    GtkWidget *clist_swin;
    GtkPaned *pane;
    GtkWidget *hbox;

    GtkCTreeNode *rootSiblingList;
    GtkCTreeNode *rootParent;
    
    GHashTable *groupHash;
    gchar *currentGroup;
    
    SortTypes sortBy;
    gboolean viewGroups;
    gboolean inNotebook;
    gint lastScanned;
};

/**************************************************************************/

#define GFREE(x)  if (x) { g_free(x) ; x = NULL; }
#define FREE(x)  if (x) { free(x) ; x = NULL; }

/**************************************************************************/

/* repo.c */

GrpmRepo *grpmRepoNew(GrpmRepoType type, gchar *ref, gchar *cache,
		      GrpmRpmdb *installedRpmdb, gboolean isRemovable,
		      gchar *name);
void grpmRepoDestroy(GrpmRepo *repo);
gint grpmRepoScan(GrpmRepo *repo);
gint grpmRepoReload(GrpmRepo *repo);
gint grpmRepoSyncCache(GrpmRepo *repo);
gint grpmRepoGetPackageFileName(GrpmRepo *repo, GrpmPackage *pkg,
				gchar *fileName);
gint grpmRepoOpenSession(GrpmRepo *repo);
void grpmRepoCloseSession(GrpmRepo *repo);

Header grpmRepoCopyInterestingHeaderBits(Header h);
void grpmRepoResetPackageStates(GrpmRepo *repo);

/* rpmdb.c */

GrpmRpmdb *grpmRpmdbNew(void);
void grpmRpmdbDestroy(GrpmRpmdb *rdb);
gint grpmRpmdbScan(GrpmRpmdb *rdb);
gint grpmRpmdbReload(GrpmRpmdb *rdb);
void grpmRpmdbSetName(GrpmRpmdb *rdb, gchar *name);
void grpmRpmdbResetPackageStates(GrpmRpmdb *rdb);

/* action.c */

GrpmActionList *grpmActionNew(void);
void grpmActionDestroy(GrpmActionList *action);
void grpmActionAddInstallPackage(GrpmActionList *action,
				 GrpmPackageListItem *listItem);
void grpmActionAddUninstallPackage(GrpmActionList *action,
				   GrpmPackageListItem *listItem);
void grpmActionRemovePackage(GrpmActionList *action,
			     GrpmPackageListItem *listItem);
void grpmActionRemovePackagePkg(GrpmActionList *action, GrpmPackage *pkg);
void grpmActionCheckDepends(GrpmActionList *action);
void grpmActionInstall(GrpmActionList *action);

/* collection.c */

GrpmCollection *grpmCollectionNew(gchar *name);
void grpmCollectionDestroy(GrpmCollection *collection);
void grpmCollectionAddPackage(GrpmCollection *collection,
			      GrpmPackageListItem *listItem);
void grpmCollectionRemovePackage(GrpmCollection *collection,
				 GrpmPackageListItem *listItem);

/* packagelist.c */

/* In these prototypes, isRepo means is a repo or a rpmdb */

GrpmPackageList *grpmPackageListNew(void);
void grpmPackageListFree(GrpmPackageList *list, gboolean isRepo);
GrpmPackageListItem *grpmPackageListAddPackage(GrpmPackageList *list,
					       GrpmPackage *pkg);

/* Should only be called by actions or collections */
void grpmPackageListRemovePackage(GrpmPackageList *list, GrpmPackage *pkg);

GrpmPackageListItem *grpmPackageListEntryFindByRef(GrpmPackageList *list,
						   gchar *ref);
GrpmPackageListItem *grpmPackageListEntryFindByName(GrpmPackageList *list,
						    gchar *name);
void grpmPackageListUnset(GrpmPackageList *list, gint flag);
gint grpmPackageListRemoveUnset(GrpmPackageList *list, gint flag,
				gboolean isRepo);

void grpmPackageListItemSet(GrpmPackageListItem *item, gint flag);
void grpmPackageListItemUnset(GrpmPackageListItem *item, gint flag);

/* panel.c */

extern GList *panelList;
extern GrpmPanel *installedPanel, *actionPanel;

GrpmPanel *grpmPanelNew(GrpmRepo *repo,
			GrpmCollection *collection,
			GrpmRpmdb *rdb,
			GrpmActionList *action,
			gchar *name,
			SortTypes sortBy,
			gboolean viewGroups,
			gchar *currentGroup,
			GrpmPanel *actionPanel,
			GrpmPanelDestroyFunc destroyFunc);
void grpmPanelDestroy(GrpmPanel *panel);
#define grpmPanelNewRepo(repo, name, sort, groups, currentGroup, \
			 action, destroy) \
                   grpmPanelNew(repo, NULL, NULL, NULL, name, sort, \
				groups, currentGroup, action, destroy)
#define grpmPanelNewCollection(collection, name, action, destroy) \
                   grpmPanelNew(NULL, collection, NULL, NULL, name, \
				SORT_STATE, FALSE, NULL, action, destroy)
#define grpmPanelNewRpmdb(rdb, name, sort, groups, currentGroup, \
			  action, destroy) \
                   grpmPanelNew(NULL, NULL, rdb, NULL, name, sort, \
				groups, currentGroup, action, destroy)
#define grpmPanelNewAction(action, name, sort, groups, currentGroup, destroy) \
                   grpmPanelNew(NULL, NULL, NULL, action, name, sort, \
				groups, currentGroup, NULL, destroy)

void grpmPanelRedraw(GrpmPanel *panel);
gint grpmPanelScan(GrpmPanel *panel);
gint grpmPanelReload(GrpmPanel *panel);
void grpmPanelResetStates(void);

/* query.c */

void grpmQueryWindow(GList *list);

/* grpm.c */

/* XXX this section shouldn't be here - there is a breakdown in */
/* the structure of the source files.                           */

typedef gint (*makeCollectionCB)(GrpmCollection *collection);

/* search.c */

void grpmSearchWindowNew(makeCollectionCB collection);

/* color.c */

extern GdkColor unknownColor;
extern GdkColor installedColor;
extern GdkColor uninstalledColor;
extern GdkColor oldpackageColor;
extern GdkColor newpackageColor;

void grpmColorInit(void);
void grpmColorLegend(void);

#endif _GRPM_H_
