/*

* freeing packages (repo.c, rpmdb.c, packagelist.c)
  packagelist.c grpmPackageListFree() is the key
  as well as grpmPackageListRemoveUnset()

* doing scan/reload in repos can invalidate other lists.

* These are almost the same problem.

* non-repos are collections, action lists, query and search results

The issue is that we could easily free the packages and move on.
*Except* other non-repo PackageLists could have PackageListItems
whose pkg element points to what we just freed.

What we want to do is to remove references from those non-repo
PackageLists to any package that has been removed from the list
in question.  On a reload this may mean that a reference to a
package was removed even though the package was subsequently
re-scanned.  For simple coding, we can live with this.  What
we'll do is if 1) a user clicks rescan on a repo, and 2) some
non-repo list contains items from that repo, then we'll put up
warning dialog suggesting a scan instead.

Need a function that scans non-repos looking for items from a certain
repo, and optionally removing them.

New rule: only repos can actually free a package.  Others just
remove their reference.

*/

#include "grpm.h"

static void grpmPackageListItemFree(GrpmPackageListItem *item);
static void grpmPackageListItemFreeAll(GrpmPackageListItem *item);

GrpmPackageList *grpmPackageListNew(void)
{
    GrpmPackageList *res;

    res = g_new(GrpmPackageList, 1);
    res->packages = NULL;
    res->refHash = g_hash_table_new(g_str_hash, g_str_equal);
    res->nameHash = g_hash_table_new(g_str_hash, g_str_equal);
    
    return res;
}

void grpmPackageListFree(GrpmPackageList *list, gboolean isRepo)
{
    if (! list) {
	return;
    }

    g_list_foreach(list->packages,
		   (isRepo) ? (GFunc) grpmPackageListItemFreeAll
		            : (GFunc) grpmPackageListItemFree,
		   NULL);
    g_list_free(list->packages);
    g_hash_table_destroy(list->refHash);
    g_hash_table_destroy(list->nameHash);
    
    /* XXX magic freeing stuff here ? */

    g_free(list);
}

GrpmPackageListItem *grpmPackageListAddPackage(GrpmPackageList *list,
					       GrpmPackage *pkg)
{
    GrpmPackageListItem *item;

    item = g_new(GrpmPackageListItem, 1);
    item->flags = 0;
    item->pkg = pkg;

    list->packages = g_list_append(list->packages, item);
    g_hash_table_insert(list->refHash, pkg->ref, item);
    g_hash_table_insert(list->nameHash, pkg->name, item);

    return item;
}

void grpmPackageListRemovePackage(GrpmPackageList *list, GrpmPackage *pkg)
{
    GrpmPackageListItem *item;
    GList *listItemPtr;

    item = grpmPackageListEntryFindByRef(list, pkg->ref);
    if (! item) {
	return;
    }

    g_hash_table_remove(list->refHash, pkg->ref);
    g_hash_table_remove(list->nameHash, pkg->name);
    if (! (listItemPtr = g_list_find(list->packages, item))) {
	return;
    }
    list->packages = g_list_remove_link(list->packages, listItemPtr);
    g_list_free(listItemPtr);
}

GrpmPackageListItem *grpmPackageListEntryFindByRef(GrpmPackageList *list,
						   gchar *ref)
{
    return g_hash_table_lookup(list->refHash, ref);
}

GrpmPackageListItem *grpmPackageListEntryFindByName(GrpmPackageList *list,
						    gchar *name)
{
    return g_hash_table_lookup(list->nameHash, name);
}

void grpmPackageListUnset(GrpmPackageList *list, gint flag)
{
    g_list_foreach(list->packages, (GFunc) grpmPackageListItemUnset,
		   GINT_TO_POINTER (flag));
}

gint grpmPackageListRemoveUnset(GrpmPackageList *packageList, gint flag,
				gboolean isRepo)
{
    GList *list, *next;
    GrpmPackageListItem *listItem;
    gint res = 0;

    list = packageList->packages;
    while (list) {
	listItem = (GrpmPackageListItem *) list->data;
	next = list->next;
	
	if (! (listItem->flags & flag)) {
	    g_hash_table_remove(packageList->refHash, listItem->pkg->ref);
	    g_hash_table_remove(packageList->nameHash, listItem->pkg->name);
	    packageList->packages =
		g_list_remove_link(packageList->packages, list);
	    g_list_free(list);

	    if (isRepo) {
		grpmPackageListItemFreeAll(listItem);
	    } else {
		grpmPackageListItemFree(listItem);
	    }
	    
	    res++;
	}
	
	list = next;
    }

    /* XXX magic freeing stuff here ? */

    return res;
}

/***************************************************************************/

/* List items */

void grpmPackageListItemSet(GrpmPackageListItem *item, gint flag)
{
    item->flags |= flag;
}

void grpmPackageListItemUnset(GrpmPackageListItem *item, gint flag)
{
    item->flags &= ~flag;
}

static void grpmPackageListItemFreeAll(GrpmPackageListItem *item)
{
    headerFree(item->pkg->header);
    GFREE(item->pkg->ref);
    GFREE(item->pkg->name);
    g_free(item->pkg);

    g_free(item);
}

static void grpmPackageListItemFree(GrpmPackageListItem *item)
{
    g_free(item);
}

