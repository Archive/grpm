#include <string.h>

#include "grpm.h"

static void grpmPanelConstruct(GrpmPanel *panel);
static void grpmPanelAppendEntry(GrpmPackageListItem *item, GrpmPanel *panel);
static void grpmPanelScanInt(void *button, GrpmPanel *panel);
static void grpmPanelReloadInt(void *button, GrpmPanel *panel);
static void grpmPanelRedrawAux(GrpmPanel *panel);

static void grpmPanelDeleteCB(void *button, GrpmPanel *panel);
static void grpmPanelInstallCB(void *button, GrpmPanel *panel);
static void grpmPanelUninstallCB(void *button, GrpmPanel *panel);
static void grpmPanelRemoveCB(void *button, GrpmPanel *panel);

static void grpmPanelCheckDependsCB(void *button, GrpmPanel *panel);
static void grpmPanelActionInstallCB(void *button, GrpmPanel *panel);

static void grpmPanelDeleteInt(GtkWidget *button, GrpmPanel *panel);
static void grpmPanelSetView(GrpmPanel *panel);
static void grpmPanelSetViewCB(GtkWidget *menu, GrpmPanel *panel);
static void grpmPanelSelectSort(GtkWidget *menu, GrpmPanel *panel);
static void grpmPanelSort(GrpmPanel *panel);
static gint grpmPanelSortByName(GrpmPackageListItem *a,
				GrpmPackageListItem *b);
static gint grpmPanelSortBySize(GrpmPackageListItem *a,
				GrpmPackageListItem *b);
static gint grpmPanelSortByDate(GrpmPackageListItem *a,
				GrpmPackageListItem *b);
static gint grpmPanelSortByInstallDate(GrpmPackageListItem *a,
				       GrpmPackageListItem *b);
static gint grpmPanelSortByState(GrpmPackageListItem *a,
				 GrpmPackageListItem *b);
static gint grpmPanelSortByStateReverse(GrpmPackageListItem *a,
					GrpmPackageListItem *b);
static void grpmPanelInitPixmaps(void);
static void grpmPanelCTreeClicked(GtkCTree *ctree, GtkCTreeNode *row,
				  gint column, GrpmPanel *panel);
static gint grpmPanelCTreeCompare(GtkCTree *ctree,
				  GtkCTreeNode *a, GtkCTreeNode *b);

static void grpmPanelRebuildGroupTree(GrpmPanel *panel);
static void grpmPanelFreeGroupHash(gchar *key, gpointer value, gpointer data);
static GtkCTreeNode *grpmPanelGroupTreeAddGroup(GrpmPanel *panel,
						gchar *group);

static GdkPixmap *closedBookPixmap;
static GdkPixmap *openBookPixmap;
static GdkBitmap *closedBookMask;
static GdkBitmap *openBookMask;

struct rowData {
    gchar *group;
    GtkCTreeNode *siblingList;
};

GList *panelList = NULL;
GrpmPanel *installedPanel, *actionPanel;

GrpmPanel *grpmPanelNew(GrpmRepo *repo,
			GrpmCollection *collection,
			GrpmRpmdb *rdb,
			GrpmActionList *action,
			gchar *name,
			SortTypes sortBy,
			gboolean viewGroups,
			gchar *currentGroup,
			GrpmPanel *actionPanel,
			GrpmPanelDestroyFunc destroyFunc)
{
    GrpmPanel *res;

    grpmColorInit();
    
    res = g_new(GrpmPanel, 1);

    res->repo = repo;
    res->collection = collection;
    res->rdb = rdb;
    res->actionList = action;

    res->destroyFunc = destroyFunc;
    res->sortBy = sortBy;
    res->viewGroups = viewGroups;
    res->groupHash = g_hash_table_new(g_str_hash, g_str_equal);
    res->currentGroup = NULL;
    res->actionPanel = actionPanel;
    if (currentGroup && *currentGroup) {
	res->currentGroup = g_strdup(currentGroup);
    }
    if (repo) {
	res->list = repo->list;
	repo->panel = res;
    } else if (collection) {
	res->list = collection->list;
    } else if (rdb) {
	res->list = rdb->list;
    } else {
	res->list = action->list;
    }
    res->name = g_strdup(name);

    grpmPanelConstruct(res);
    grpmPanelInitPixmaps();
    grpmPanelRebuildGroupTree(res);
    grpmPanelSort(res);     /* This actually constructs the clist */
                            /* and does a redraw                  */

    if (! res->viewGroups) {
	grpmPanelSetView(res);
    }

    return res;
}

void grpmPanelDestroy(GrpmPanel *panel)
{
    if (panel->destroyFunc) {
	(panel->destroyFunc)(panel);
    }
    gtk_widget_destroy(panel->widget);

    g_hash_table_destroy(panel->groupHash);
    GFREE(panel->currentGroup);

    if (panel->repo) {
	grpmRepoDestroy(panel->repo);
    } else if (panel->rdb) {
	grpmRpmdbDestroy(panel->rdb);
    } else if (panel->actionList) {
	grpmActionDestroy(panel->actionList);
    } else if (panel->collection) {
	grpmCollectionDestroy(panel->collection);
    }
    
    g_free(panel->name);
    g_free(panel);
}

gint grpmPanelScan(GrpmPanel *panel)
{
    gint res;

    if (panel->collection || panel->actionList) {
	return 0;
    }

    if (panel->repo) {
	res = grpmRepoScan(panel->repo);
    } else {
	res = grpmRpmdbScan(panel->rdb);
    }

    grpmPanelRebuildGroupTree(panel);
    grpmPanelResetStates();
    
    return res;
}

gint grpmPanelReload(GrpmPanel *panel)
{
    gint res;

    if (panel->actionList) {
	return 0;
    }

    if (panel->repo) {
	res = grpmRepoReload(panel->repo);
	panel->list = panel->repo->list;
    } else {
	res = grpmRpmdbReload(panel->rdb);
	panel->list = panel->rdb->list;
    }

    grpmPanelRebuildGroupTree(panel);
    grpmPanelResetStates();

    return res;
}

static void grpmPanelScanInt(void *button, GrpmPanel *panel)
{
    grpmPanelScan(panel);
}

static void grpmPanelReloadInt(void *button, GrpmPanel *panel)
{
    grpmPanelReload(panel);
}

static void grpmPanelFreeGroupHash(gchar *key, gpointer value, gpointer data)
{
    g_free(key);
}

static void grpmPanelRebuildGroupTree(GrpmPanel *panel)
{
    GList *list;
    GrpmPackageListItem *item;
    gchar *group;
    gchar *text[1] = { "All Packages" };
    struct rowData *rd = NULL;

    gtk_clist_freeze(GTK_CLIST(panel->ctree));
    gtk_clist_clear(GTK_CLIST(panel->ctree));
    
    g_hash_table_foreach(panel->groupHash,
			 (GHFunc)grpmPanelFreeGroupHash, NULL);
    g_hash_table_destroy(panel->groupHash);
    panel->groupHash = g_hash_table_new(g_str_hash, g_str_equal);

    panel->rootSiblingList = NULL;
    panel->rootParent = gtk_ctree_insert_node(panel->ctree, NULL, NULL, text, 5,
					 closedBookPixmap, closedBookMask,
					 openBookPixmap, openBookMask,
					 FALSE, TRUE);
    rd = g_new(struct rowData, 1);
    rd->group = NULL;
    rd->siblingList = NULL;
    gtk_ctree_node_set_row_data_full(panel->ctree, panel->rootParent, rd, g_free);
    
    list = panel->list->packages;
    while (list) {
	item = (GrpmPackageListItem *) list->data;
	headerGetEntry(item->pkg->header, RPMTAG_GROUP, NULL,
		       (void **)&group, NULL);
	grpmPanelGroupTreeAddGroup(panel, group);
	
	list = list->next;
    }
    
    gtk_clist_thaw(GTK_CLIST(panel->ctree));
}

static GtkCTreeNode *grpmPanelGroupTreeAddGroup(GrpmPanel *panel, gchar *group)
{
    gchar *p;
    GtkCTreeNode *res, *parent, *sibling;
    gchar buf[BUFSIZ];
    gchar *base;
    gchar *text[1];
    struct rowData *rd = NULL;
    gboolean expanded = FALSE;
    
    if ((res = g_hash_table_lookup(panel->groupHash, group))) {
	return res;
    }

    if ((base = strrchr(group, '/'))) {
	/* Need to recurse */
	strcpy(buf, group);
	buf[base - group] = '\0';
	parent = grpmPanelGroupTreeAddGroup(panel, buf);
	base++;
    } else {
	/* A root group */
	parent = NULL;
	base = group;
    }

    sibling = NULL;
    if (parent) {
      rd = gtk_ctree_node_get_row_data(panel->ctree, parent);
	sibling = rd->siblingList;
    } else {
	sibling = panel->rootSiblingList;
    }
    
    /* assert: parent, base are set properly */

    if (panel->currentGroup) {
	if (! strncmp(group, panel->currentGroup, strlen(group))) {
	    expanded = TRUE;
	}
    }
    
    /* XXX This should insert in alpha order */
    text[0] = base;
    res = gtk_ctree_insert_node(panel->ctree,
			   parent ? parent : panel->rootParent,
			   sibling, text, 5,
			   closedBookPixmap, closedBookMask,
			   openBookPixmap, openBookMask,
			   FALSE, expanded);

    if (panel->currentGroup) {
	if (! strcmp(group, panel->currentGroup)) {
	    /* This is the selected group - select it */
	    gtk_signal_disconnect_by_func(GTK_OBJECT(panel->ctree),
			       GTK_SIGNAL_FUNC(grpmPanelCTreeClicked), panel);
	    gtk_ctree_select(panel->ctree, res);
	    gtk_signal_connect(GTK_OBJECT(panel->ctree), "tree_select_row",
			       GTK_SIGNAL_FUNC(grpmPanelCTreeClicked), panel);
	}
    }

    if (parent) {
	rd->siblingList = res;
    } else {
	panel->rootSiblingList = res;
    }
    
    p = g_strdup(group);
    rd = g_new(struct rowData, 1);
    rd->group = p;
    rd->siblingList = NULL;
    
    gtk_ctree_node_set_row_data_full(panel->ctree, res, rd, g_free);
    g_hash_table_insert(panel->groupHash, p, res);

    return res;
}

/* This should only ever be called if the geometry is really       */
/* changing.  Calling this twice in a row without changing the     */
/* value of viewGroups will result in, well, bad things happening. */
static void grpmPanelSetView(GrpmPanel *panel)
{
    if (panel->viewGroups) {
	gtk_object_ref(GTK_OBJECT(panel->clist_swin));
	gtk_container_remove(GTK_CONTAINER(panel->hbox), panel->clist_swin);
	gtk_paned_add2(panel->pane, GTK_WIDGET(panel->clist_swin));
	gtk_object_unref(GTK_OBJECT(panel->clist_swin));
	gtk_widget_show(GTK_WIDGET(panel->pane));
    } else {
	gtk_widget_hide(GTK_WIDGET(panel->pane));
	gtk_object_ref(GTK_OBJECT(panel->clist_swin));
	gtk_container_remove(GTK_CONTAINER(panel->pane), panel->clist_swin);
	gtk_box_pack_start(GTK_BOX(panel->hbox), panel->clist_swin,
			   TRUE, TRUE, 0);
	gtk_object_unref(GTK_OBJECT(panel->clist_swin));
    }
}

static void grpmPanelSetViewCB(GtkWidget *menu, GrpmPanel *panel)
{
    gboolean viewGroups;
    
    viewGroups = gtk_object_get_user_data(GTK_OBJECT(menu)) ? TRUE : FALSE;
    
    if (viewGroups == panel->viewGroups) {
	return;
    }

    panel->viewGroups = viewGroups;
    grpmPanelSetView(panel);
    grpmPanelRedraw(panel);
}

void grpmPanelResetStates(void)
{
    GList *list;
    GrpmPanel *panel;

    grpmRpmdbResetPackageStates(installedPanel->rdb);
    
    list = panelList;
    while (list) {
	panel = (GrpmPanel *) list->data;
	if (panel->repo) {
	    grpmRepoResetPackageStates(panel->repo);
	    grpmPanelSort(panel);
	}
	list = list->next;
    }

    grpmPanelSort(installedPanel);
}

void grpmPanelRedraw(GrpmPanel *panel)
{
    GList *list;
    
    if (panel) {
	grpmPanelRedrawAux(panel);
    } else {
	list = panelList;
	while (list) {
	    grpmPanelRedrawAux((GrpmPanel *) list->data);
	    list = list->next;
	}
    }
}

static void grpmPanelRedrawAux(GrpmPanel *panel)
{
    gint toprow = 0;
    
    gtk_clist_freeze(GTK_CLIST(panel->clist));

    if (GTK_WIDGET_REALIZED(GTK_CLIST(panel->clist))) {
	while (gtk_clist_row_is_visible(GTK_CLIST(panel->clist), toprow)
	       == GTK_VISIBILITY_NONE
	       && toprow < GTK_CLIST(panel->clist)->rows) {
	    toprow++;
	}
    }
    
    gtk_clist_clear(GTK_CLIST(panel->clist));

    g_list_foreach(panel->list->packages,
		   (GFunc) grpmPanelAppendEntry, panel);

    if (GTK_WIDGET_REALIZED(GTK_CLIST(panel->clist))) {
	gtk_clist_moveto(GTK_CLIST(panel->clist), toprow, -1, 0.0, 0.0);
    }
    
    gtk_clist_thaw(GTK_CLIST(panel->clist));
}

static void grpmPanelAppendEntry(GrpmPackageListItem *item, GrpmPanel *panel)
{
    gint x;
    gchar *name, *version, *release, *summary, *group;
    gchar verBuf[BUFSIZ];
    gchar nameBuf[BUFSIZ];
    gchar *row[4];
    GdkColor *color;

    /* Check panel->currentGroup */
    headerGetEntry(item->pkg->header, RPMTAG_GROUP, NULL,
		   (void **)&group, NULL);
    if (panel->viewGroups) {
	if (panel->currentGroup) {
	    if (group && strncmp(panel->currentGroup, group,
				 strlen(panel->currentGroup))) {
		return;
	    }
	}
    }
    
    headerGetEntry(item->pkg->header, RPMTAG_NAME, NULL,
		   (void **)&name, NULL);
    headerGetEntry(item->pkg->header, RPMTAG_VERSION, NULL,
		   (void **)&version, NULL);
    headerGetEntry(item->pkg->header, RPMTAG_RELEASE, NULL,
		   (void **)&release, NULL);
    headerGetEntry(item->pkg->header, RPMTAG_SUMMARY, NULL,
		   (void **)&summary, NULL);
    
    g_snprintf(verBuf, sizeof(verBuf), "%s-%s", version, release);

    row[0] = name;
    row[1] = verBuf;
    if (panel->actionList || panel->collection) {
	if (item->pkg->repo) {
	    row[2] = item->pkg->repo->name;
	} else {
	    row[2] = "Installed";
	}
	row[3] = summary;
    } else {
	row[2] = summary;
    }

    /* XXX set icon instead of this text */
    if (item->pkg->doWhat == GRPM_PACKAGE_DOWHAT_INSTALL) {
	g_snprintf(nameBuf, sizeof(nameBuf), "IN %s", name);
	row[0] = nameBuf;
    } else if (item->pkg->doWhat == GRPM_PACKAGE_DOWHAT_UNINSTALL) {
	g_snprintf(nameBuf, sizeof(nameBuf), "UN %s", name);
	row[0] = nameBuf;
    }
    
    x = gtk_clist_append(GTK_CLIST(panel->clist), row);
    gtk_clist_set_row_data(GTK_CLIST(panel->clist), x, item);

    switch (item->pkg->state) {
      case GRPM_PACKAGE_STATE_UNKNOWN:
	color = &unknownColor;
	break;
      case GRPM_PACKAGE_STATE_INSTALLED:
	color = &installedColor;
	break;
      case GRPM_PACKAGE_STATE_UNINSTALLED:
	color = &uninstalledColor;
	break;
      case GRPM_PACKAGE_STATE_OLDPACKAGE:
	color = &oldpackageColor;
	break;
      case GRPM_PACKAGE_STATE_NEWPACKAGE:
	color = &newpackageColor;
	break;
      default:
	color = &unknownColor;
	break;
    }

    gtk_clist_set_background(GTK_CLIST(panel->clist), x, color);
}

static void grpmPanelDeleteCB(void *button, GrpmPanel *panel)
{
    GtkWidget *dialog, *label, *vbox;
    
    dialog = gnome_dialog_new(panel->repo ?
			      "Delete Repo" : "Delete Collection",
			      GNOME_STOCK_BUTTON_OK,
			      GNOME_STOCK_BUTTON_CANCEL,
			      NULL);

    gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
    gnome_dialog_set_close(GNOME_DIALOG(dialog), TRUE);

    label = gtk_label_new("Really delete?");
    gtk_widget_show(label);

    vbox = GNOME_DIALOG(dialog)->vbox;
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 2);

    gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0,
				(GtkSignalFunc)grpmPanelDeleteInt, panel);
    
    gtk_widget_show(dialog);
}

static gint grpmPanelSortByName(GrpmPackageListItem *a, GrpmPackageListItem *b)
{
    char *namea, *nameb;

    headerGetEntry(a->pkg->header, RPMTAG_NAME, NULL,
		   (void **) &namea, NULL);
    headerGetEntry(b->pkg->header, RPMTAG_NAME, NULL,
		   (void **) &nameb, NULL);
    
    return strcmp(namea, nameb);
}

static gint grpmPanelSortBySize(GrpmPackageListItem *a, GrpmPackageListItem *b)
{
    gint *sizea, *sizeb;

    headerGetEntry(a->pkg->header, RPMTAG_SIZE, NULL,
		   (void **) &sizea, NULL);
    headerGetEntry(b->pkg->header, RPMTAG_SIZE, NULL,
		   (void **) &sizeb, NULL);

    return *sizeb - *sizea;
}

static gint grpmPanelSortByDate(GrpmPackageListItem *a, GrpmPackageListItem *b)
{
    gint *datea, *dateb;

    headerGetEntry(a->pkg->header, RPMTAG_BUILDTIME, NULL,
		   (void **) &datea, NULL);
    headerGetEntry(b->pkg->header, RPMTAG_BUILDTIME, NULL,
		   (void **) &dateb, NULL);

    return *dateb - *datea;
}

static gint grpmPanelSortByInstallDate(GrpmPackageListItem *a,
				       GrpmPackageListItem *b)
{
    gint *datea, *dateb;

    headerGetEntry(a->pkg->header, RPMTAG_INSTALLTIME, NULL,
		   (void **) &datea, NULL);
    headerGetEntry(b->pkg->header, RPMTAG_INSTALLTIME, NULL,
		   (void **) &dateb, NULL);

    /* The basesystem package has no installtime for some reason */
    if (!datea) {
	return 1;
    }
    if (!dateb) {
	return -1;
    }
    
    return *dateb - *datea;
}

static gint grpmPanelSortByState(GrpmPackageListItem *a,
				 GrpmPackageListItem *b)
{
    return a->pkg->state < b->pkg->state;
}

static gint grpmPanelSortByStateReverse(GrpmPackageListItem *a,
					GrpmPackageListItem *b)
{
    return a->pkg->state > b->pkg->state;
}

static void grpmPanelSort(GrpmPanel *panel)
{
    GList *newList = NULL;
    GList *list;
    GCompareFunc compareFunc;
    
    switch (panel->sortBy) {
      case SORT_NAME:
	compareFunc = (GCompareFunc)grpmPanelSortByName;
	break;
      case SORT_SIZE:
	compareFunc = (GCompareFunc)grpmPanelSortBySize;
	break;
      case SORT_DATE:
	if (panel->rdb) {
	    compareFunc = (GCompareFunc)grpmPanelSortByInstallDate;
	} else {
	    compareFunc = (GCompareFunc)grpmPanelSortByDate;
	}
	break;
      case SORT_STATE:
	if (panel->rdb) {
	    compareFunc = (GCompareFunc)grpmPanelSortByStateReverse;
	} else {
	    compareFunc = (GCompareFunc)grpmPanelSortByState;
	}
	break;
      default:
	g_error("unknown sort order %d.  this should never happen",
		panel->sortBy);
	return;
    }
    
    list = panel->list->packages;
    while (list) {
	newList = g_list_insert_sorted(newList, list->data, compareFunc);
	list = list->next;
    }

    list = panel->list->packages;
    panel->list->packages = newList;
    g_list_free(list);

    grpmPanelRedraw(panel);
}

static void grpmPanelSelectSort(GtkWidget *menu, GrpmPanel *panel)
{
    SortTypes sortType;

    sortType = (SortTypes) gtk_object_get_user_data(GTK_OBJECT(menu));

    if (sortType != panel->sortBy) {
	panel->sortBy = sortType;
	grpmPanelSort(panel);
    }
}

static void grpmPanelDeleteInt(GtkWidget *button, GrpmPanel *panel)
{
    grpmPanelDestroy(panel);
}

static gint grpmPanelCTreeCompare(GtkCTree *ctree,
				  GtkCTreeNode *a, GtkCTreeNode *b)
{
    struct rowData *rda, *rdb;

    if (!a || !b) {
	return 0;
    }
    
    rda = gtk_ctree_node_get_row_data(ctree, a);
    rdb = gtk_ctree_node_get_row_data(ctree, b);
        
    if (!rda || !rdb) {
	return 0;
    }

    return strcasecmp(rda->group, rdb->group);
}

static void grpmPanelCTreeClicked(GtkCTree *ctree, GtkCTreeNode *row,
				  gint column, GrpmPanel *panel)
{
    struct rowData *rd;
    
    rd = gtk_ctree_node_get_row_data(ctree, row);
    GFREE(panel->currentGroup);
    if (rd->group) {
	panel->currentGroup = g_strdup(rd->group);
    } else {
	panel->currentGroup = NULL;
    }

    grpmPanelRedraw(panel);
}

static void grpmPanelInstallCB(void *button, GrpmPanel *panel)
{
    GList *list;
    GrpmPackageListItem *item;
    
    list = GTK_CLIST(panel->clist)->selection;
    while (list) {
	item = gtk_clist_get_row_data(GTK_CLIST(panel->clist),
				      GPOINTER_TO_INT (list->data));
	grpmActionAddInstallPackage(panel->actionPanel->actionList, item);
	list = list->next;
    }
    
    /* Redraw affected panels */
    grpmPanelRedraw(panel->actionPanel);
    grpmPanelRebuildGroupTree(panel->actionPanel);
    grpmPanelRedraw(panel);
}

static void grpmPanelUninstallCB(void *button, GrpmPanel *panel)
{
    GList *list;
    GrpmPackageListItem *item;
    
    list = GTK_CLIST(panel->clist)->selection;
    while (list) {
	item = gtk_clist_get_row_data(GTK_CLIST(panel->clist),
				      GPOINTER_TO_INT (list->data));
	grpmActionAddUninstallPackage(panel->actionPanel->actionList, item);
	list = list->next;
    }
    
    /* Redraw affected panels */
    grpmPanelRedraw(panel->actionPanel);
    grpmPanelRebuildGroupTree(panel->actionPanel);
    grpmPanelRedraw(panel);
}

static void grpmPanelQueryCB(void *button, GrpmPanel *panel)
{
    GList *list, *newList;
    GrpmPackageListItem *item;
    
    newList = NULL;
    list = GTK_CLIST(panel->clist)->selection;
    while (list) {
	item = gtk_clist_get_row_data(GTK_CLIST(panel->clist),
				      GPOINTER_TO_INT (list->data));
	newList = g_list_append(newList, item);
	list = list->next;
    }

    grpmQueryWindow(newList);
    g_list_free(newList);
}

static void grpmPanelRemoveCB(void *button, GrpmPanel *panel)
{
    GList *list;
    GrpmPackageListItem *item;
    
    list = GTK_CLIST(panel->clist)->selection;
    while (list) {
	item = gtk_clist_get_row_data(GTK_CLIST(panel->clist),
				      GPOINTER_TO_INT (list->data));
	if (panel->actionList) {
	    grpmActionRemovePackage(panel->actionList, item);
	} else {
	    grpmCollectionRemovePackage(panel->collection, item);
	}
	list = list->next;
    }
    
    /* Rebuild this panel's group tree */
    grpmPanelRebuildGroupTree(panel);

    /* Redraw ALL panels.                                            */
    /* This is overkill, but there is currently not an easy way      */
    /* to map the removed packages back to the panels they belong to */
    grpmPanelRedraw(NULL);
}

static void grpmPanelCheckDependsCB(void *button, GrpmPanel *panel)
{
    grpmActionCheckDepends(panel->actionList);
}

static void grpmPanelActionInstallCB(void *button, GrpmPanel *panel)
{
    grpmActionInstall(panel->actionList);
}

static GtkWidget * grpmPanelConstructContextMenu(GrpmPanel *panel)
{
    GtkWidget *menu, *item;

    menu = gtk_menu_new();

    item = gtk_menu_item_new_with_label("Query");
    gtk_signal_connect(GTK_OBJECT(item), "activate",
		       GTK_SIGNAL_FUNC(grpmPanelQueryCB), panel);
    gtk_menu_append(GTK_MENU(menu), item);
    gtk_widget_show(item);

    if (panel->repo || panel->collection) {
	item = gtk_menu_item_new_with_label("Install");
	gtk_signal_connect(GTK_OBJECT(item), "activate",
			   GTK_SIGNAL_FUNC(grpmPanelInstallCB), panel);
	gtk_menu_append(GTK_MENU(menu), item);
	gtk_widget_show(item);
    }

    if (panel->rdb) {
	item = gtk_menu_item_new_with_label("Uninstall");
	gtk_signal_connect(GTK_OBJECT(item), "activate",
			   GTK_SIGNAL_FUNC(grpmPanelUninstallCB), panel);
	gtk_menu_append(GTK_MENU(menu), item);
	gtk_widget_show(item);
    }

    if (panel->actionList || panel->collection) {
	item = gtk_menu_item_new_with_label("Remove");
	gtk_signal_connect(GTK_OBJECT(item), "activate",
			   GTK_SIGNAL_FUNC(grpmPanelRemoveCB), panel);
	gtk_menu_append(GTK_MENU(menu), item);
	gtk_widget_show(item);
    }

    return menu;
}

static gint grpmPanelCListClickCB(GtkWidget *widget, GdkEventButton *event,
				  gpointer data)
{
    GrpmPanel *panel = (GrpmPanel *) data;
    GtkWidget *menu;
    gint row, col;
    
    if (event->type == GDK_BUTTON_PRESS && event->button == 3) {
	if (gtk_clist_get_selection_info(GTK_CLIST(panel->clist), event->x,
					 event->y, &row, &col)) {
	    if (g_list_find(GTK_CLIST(panel->clist)->selection, 
			    GINT_TO_POINTER (row)) == NULL) {
		gtk_clist_unselect_all(GTK_CLIST(panel->clist));
		gtk_clist_select_row(GTK_CLIST(panel->clist), row, col);
	    }
	    menu = grpmPanelConstructContextMenu(panel);
	    gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL,
			   3, event->time);
	    return TRUE;
	}
    }

    return FALSE;
}

static void grpmPanelConstruct(GrpmPanel *panel)
{
    GtkWidget *hbox;
    gchar *titles[3] = { "Name", "Version", "Summary" };
    gchar *titlesExtra[4] = { "Name", "Version", "Repo", "Summary" };
    gchar *title[1] = { "Group" };
    char *items[] = { "Name", "Size", "Date", "State", NULL };
    char **s;
    GtkWidget *item, *sortMenu, *menu, *groupMenu, *icon = NULL;
    GtkToolbar *toolbar;
    SortTypes sortType;

    /* Vbox */
    panel->widget = gtk_vbox_new(FALSE, 5);
    gtk_container_set_border_width (GTK_CONTAINER (panel->widget), 5);
    gtk_widget_show(panel->widget);

    /* Hbox for lists */
    hbox = gtk_hbox_new(FALSE, 5);
    panel->hbox = hbox;
    gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);
    gtk_box_pack_start(GTK_BOX(panel->widget), hbox, TRUE, TRUE, 0);
    gtk_widget_show(hbox);

    /* Pane */
    panel->pane = GTK_PANED(gtk_hpaned_new());
    gtk_widget_show(GTK_WIDGET(panel->pane));
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(panel->pane), TRUE, TRUE, 0);

    /* The ctree */
    panel->ctree = GTK_CTREE(gtk_ctree_new_with_titles(1, 0, title));
    gtk_clist_column_titles_passive(GTK_CLIST(panel->ctree));
    gtk_ctree_set_line_style(panel->ctree, GTK_CTREE_LINES_DOTTED);
    gtk_clist_set_reorderable (GTK_CLIST (panel->ctree), FALSE);

    gtk_clist_set_column_justification(GTK_CLIST(panel->ctree), 0,
				       GTK_JUSTIFY_LEFT);
    gtk_clist_set_selection_mode(GTK_CLIST(panel->ctree),
				 GTK_SELECTION_SINGLE);

    gtk_clist_set_column_width(GTK_CLIST(panel->ctree), 0, 75);

    /* XXX doesn't seem to work, or at least I don't understand it */
#if 0    
    gtk_ctree_set_auto_sort(panel->ctree, TRUE);
    gtk_ctree_set_compare_func(panel->ctree,
			       (GtkCTreeCompareFunc)grpmPanelCTreeCompare);
#endif

    gtk_widget_show(GTK_WIDGET(panel->ctree));

    gtk_widget_set_usize(GTK_WIDGET(panel->ctree), 200, -1);

    panel->ctree_swin = gtk_scrolled_window_new (NULL, NULL);
    gtk_container_add (GTK_CONTAINER(panel->ctree_swin),
		       GTK_WIDGET(panel->ctree));
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(panel->ctree_swin),
				    GTK_POLICY_AUTOMATIC,
				    GTK_POLICY_AUTOMATIC);

    gtk_widget_show(panel->ctree_swin);

    gtk_paned_add1(panel->pane, GTK_WIDGET(panel->ctree_swin));
    
    gtk_signal_connect(GTK_OBJECT(panel->ctree), "tree_select_row",
		       GTK_SIGNAL_FUNC(grpmPanelCTreeClicked), panel);

    gtk_widget_push_visual(gdk_imlib_get_visual());
    gtk_widget_push_colormap(gdk_imlib_get_colormap());

    /* The clist */
    if (panel->actionList || panel->collection) {
	panel->clist = gtk_clist_new_with_titles(4, titlesExtra);
    } else {
	panel->clist = gtk_clist_new_with_titles(3, titles);
    }

    gtk_widget_pop_colormap();
    gtk_widget_pop_visual(); 

    gtk_clist_set_selection_mode(GTK_CLIST(panel->clist),
				 GTK_SELECTION_MULTIPLE);

    gtk_clist_column_titles_show(GTK_CLIST(panel->clist));
    gtk_clist_column_titles_passive(GTK_CLIST(panel->clist));

    gtk_clist_set_column_justification(GTK_CLIST(panel->clist), 0,
				       GTK_JUSTIFY_LEFT);
    gtk_clist_set_column_width(GTK_CLIST(panel->clist), 0, 150);

    gtk_clist_set_column_justification(GTK_CLIST(panel->clist), 1,
				       GTK_JUSTIFY_LEFT);
    gtk_clist_set_column_width(GTK_CLIST(panel->clist), 1, 125);

    if (panel->actionList || panel->collection) {
	gtk_clist_set_column_justification(GTK_CLIST(panel->clist), 2,
					   GTK_JUSTIFY_LEFT);
	gtk_clist_set_column_width(GTK_CLIST(panel->clist), 2, 125);
	gtk_clist_set_column_justification(GTK_CLIST(panel->clist), 3,
					   GTK_JUSTIFY_LEFT);
	gtk_clist_set_column_width(GTK_CLIST(panel->clist), 3, 300);
    } else {
	gtk_clist_set_column_justification(GTK_CLIST(panel->clist), 2,
					   GTK_JUSTIFY_LEFT);
	gtk_clist_set_column_width(GTK_CLIST(panel->clist), 2, 300);
    }

    gtk_widget_show(panel->clist);

    gtk_signal_connect_after(GTK_OBJECT(panel->clist), "button_press_event",
			     GTK_SIGNAL_FUNC(grpmPanelCListClickCB),
			     (gpointer) panel);

    panel->clist_swin = gtk_scrolled_window_new (NULL, NULL);
    gtk_container_add (GTK_CONTAINER(panel->clist_swin),
		       GTK_WIDGET(panel->clist));
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(panel->clist_swin),
				    GTK_POLICY_AUTOMATIC,
				    GTK_POLICY_ALWAYS);

    gtk_widget_show(panel->clist_swin);

    gtk_paned_add2(panel->pane, GTK_WIDGET(panel->clist_swin));

    /* Buttons */

    toolbar = GTK_TOOLBAR(gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL,
					  GTK_TOOLBAR_BOTH));

    if (!panel->actionList && !panel->rdb && !panel->collection) {
	gtk_toolbar_append_item(toolbar, "Scan",
				"Scan for new packages",
				"Scan for new packages",
				icon,
				GTK_SIGNAL_FUNC(grpmPanelScanInt),
				panel);
    }
    
    if (!panel->actionList && !panel->collection) {
	gtk_toolbar_append_item(toolbar, "Reload",
				"Reload all package info",
				"Reload all package info",
				icon,
				GTK_SIGNAL_FUNC(grpmPanelReloadInt),
				panel);
    }

#if 0    
    gtk_toolbar_append_item(toolbar, "Configure",
			    "Configure this panel",
			    "Configure this panel",
			    icon,
			    GTK_SIGNAL_FUNC(NULL),
			    panel);
#endif
    
    if (!panel->rdb && !panel->actionList) {
	gtk_toolbar_append_item(toolbar, "Delete",
				"Delete this panel",
				"Delete this panel",
				icon,
				GTK_SIGNAL_FUNC(grpmPanelDeleteCB),
				panel);
    }
    
    gtk_toolbar_append_item(toolbar, "Query",
			    "Query selected packages",
			    "Query selected packages",
			    icon,
			    GTK_SIGNAL_FUNC(grpmPanelQueryCB),
			    panel);

    if (panel->repo || panel->collection) {
	gtk_toolbar_append_item(toolbar, "Install",
				"Mark selected packages for installation",
				"Mark selected packages for installation",
				icon,
				GTK_SIGNAL_FUNC(grpmPanelInstallCB),
				panel);
    }

    if (panel->rdb) {
	gtk_toolbar_append_item(toolbar, "Uninstall",
				"Mark selected packages for removal",
				"Mark selected packages for removal",
				icon,
				GTK_SIGNAL_FUNC(grpmPanelUninstallCB),
				panel);
    }

    if (panel->actionList || panel->collection) {
	gtk_toolbar_append_item(toolbar, "Remove",
				"Remove selected packages from list",
				"Remove selected packages from list",
				icon,
				GTK_SIGNAL_FUNC(grpmPanelRemoveCB),
				panel);
    }
	
    if (panel->actionList) {
	gtk_toolbar_append_item(toolbar, "Depends",
				"Resolve dependencies",
				"Resolve dependencies",
				icon,
				GTK_SIGNAL_FUNC(grpmPanelCheckDependsCB),
				panel);
	
	gtk_toolbar_append_item(toolbar, "Go",
				"Start installation",
				"Start installation",
				icon,
				GTK_SIGNAL_FUNC(grpmPanelActionInstallCB),
				panel);
    }

    /* Sorting stuff */
    sortMenu = gtk_option_menu_new();
    menu = gtk_menu_new();
    s = items;
    sortType = SORT_NAME;
    while (*s) {
	item = gtk_menu_item_new_with_label(*s);
	gtk_menu_append(GTK_MENU(menu), item);
	gtk_widget_show(item);
	gtk_signal_connect(GTK_OBJECT(item), "activate",
			  (GtkSignalFunc)grpmPanelSelectSort, panel);
	gtk_object_set_user_data(GTK_OBJECT(item), (gpointer)sortType);
	sortType++;
	s++;
    }
    gtk_option_menu_set_menu(GTK_OPTION_MENU(sortMenu), menu);
    gtk_option_menu_set_history(GTK_OPTION_MENU(sortMenu), panel->sortBy - 1);
    gtk_toolbar_append_widget(toolbar, sortMenu,
			      "Sort criteria", "Sort criteria");

    /* Group or all */
    groupMenu = gtk_option_menu_new();
    menu = gtk_menu_new();

    item = gtk_menu_item_new_with_label("All");
    gtk_menu_append(GTK_MENU(menu), item);
    gtk_widget_show(item);
    gtk_signal_connect(GTK_OBJECT(item), "activate",
		       (GtkSignalFunc)grpmPanelSetViewCB, panel);
    gtk_object_set_user_data(GTK_OBJECT(item), (gpointer)FALSE);

    item = gtk_menu_item_new_with_label("By Group");
    gtk_menu_append(GTK_MENU(menu), item);
    gtk_widget_show(item);
    gtk_signal_connect(GTK_OBJECT(item), "activate",
		       (GtkSignalFunc)grpmPanelSetViewCB, panel);
    gtk_object_set_user_data(GTK_OBJECT(item), (gpointer)TRUE);

    gtk_option_menu_set_menu(GTK_OPTION_MENU(groupMenu), menu);
    gtk_option_menu_set_history(GTK_OPTION_MENU(groupMenu), panel->viewGroups);
    gtk_toolbar_append_widget(toolbar, groupMenu,
			      "Display mode", "Display mode");

    gtk_box_pack_start(GTK_BOX(panel->widget), GTK_WIDGET(toolbar),
		       FALSE, FALSE, 0);
    gtk_widget_show_all(GTK_WIDGET(toolbar));
}

static char * book_open_xpm[] = {
"16 16 4 1",
"       c None s None",
".      c black",
"X      c #808080",
"o      c white",
"                ",
"  ..            ",
" .Xo.    ...    ",
" .Xoo. ..oo.    ",
" .Xooo.Xooo...  ",
" .Xooo.oooo.X.  ",
" .Xooo.Xooo.X.  ",
" .Xooo.oooo.X.  ",
" .Xooo.Xooo.X.  ",
" .Xooo.oooo.X.  ",
"  .Xoo.Xoo..X.  ",
"   .Xo.o..ooX.  ",
"    .X..XXXXX.  ",
"    ..X.......  ",
"     ..         ",
"                "};

static char * book_closed_xpm[] = {
"16 16 6 1",
"       c None s None",
".      c black",
"X      c red",
"o      c yellow",
"O      c #808080",
"#      c white",
"                ",
"       ..       ",
"     ..XX.      ",
"   ..XXXXX.     ",
" ..XXXXXXXX.    ",
".ooXXXXXXXXX.   ",
"..ooXXXXXXXXX.  ",
".X.ooXXXXXXXXX. ",
".XX.ooXXXXXX..  ",
" .XX.ooXXX..#O  ",
"  .XX.oo..##OO. ",
"   .XX..##OO..  ",
"    .X.#OO..    ",
"     ..O..      ",
"      ..        ",
"                "};

static void grpmPanelInitPixmaps(void)
{
    static gboolean done = FALSE;

    if (done) {
	return;
    }

    gdk_imlib_data_to_pixmap(book_closed_xpm,
			     &closedBookPixmap, &closedBookMask);
    gdk_imlib_data_to_pixmap(book_open_xpm,
			     &openBookPixmap, &openBookMask);

    done = TRUE;
}
