#include "grpm.h"

struct _query_window_struct {
    GtkWidget *window;
    GtkWidget *notebook;
};

typedef struct _query_window_struct QueryWindow;

static gboolean grpmQueryDestroy(GtkWidget *window);
static void grpmQueryAdd(QueryWindow *q, GrpmPackageListItem *item);
static void grpmQuerySetScroll(GtkAdjustment *adj, GtkWidget *scroll);

void grpmQueryWindow(GList *list)
{
    QueryWindow *queryWindow;
    GtkWidget *vbox;
    GrpmPackageListItem *item;

    if (!list) {
	return;
    }

    queryWindow = g_new(QueryWindow, 1);

    queryWindow->window = gnome_dialog_new("Query",
					   GNOME_STOCK_BUTTON_CLOSE,
					   NULL);
    gtk_window_set_policy(GTK_WINDOW(queryWindow->window), TRUE, TRUE, FALSE);
    gnome_dialog_set_close(GNOME_DIALOG(queryWindow->window), TRUE);
    
    /* Vbox */
    vbox = GNOME_DIALOG(queryWindow->window)->vbox;

    /* Notebook */
    queryWindow->notebook = gtk_notebook_new();
    gtk_notebook_set_tab_pos(GTK_NOTEBOOK(queryWindow->notebook), GTK_POS_TOP);
    gtk_notebook_set_scrollable (GTK_NOTEBOOK(queryWindow->notebook), TRUE);
    gtk_container_set_border_width(GTK_CONTAINER(queryWindow->notebook), 5);
    gtk_box_pack_start(GTK_BOX(vbox), queryWindow->notebook, TRUE, TRUE, 0);
    gtk_widget_show(queryWindow->notebook);

    /* Signals */
    gtk_signal_connect(GTK_OBJECT (queryWindow->window), "destroy",
		       GTK_SIGNAL_FUNC(grpmQueryDestroy), NULL);
    gtk_signal_connect(GTK_OBJECT (queryWindow->window), "delete_event",
		       GTK_SIGNAL_FUNC(grpmQueryDestroy), NULL);

    gtk_object_set_user_data(GTK_OBJECT(queryWindow->window), queryWindow);
    gtk_widget_show_all(queryWindow->window);

    while (list) {
	item = list->data;
	grpmQueryAdd(queryWindow, item);
	list = list->next;
    }
}

static gboolean grpmQueryDestroy(GtkWidget *window)
{
    QueryWindow *q;

    q = gtk_object_get_user_data(GTK_OBJECT(window));
    gtk_object_set_user_data(GTK_OBJECT(window), NULL);
    if (q) {
	g_free(q);
    }
    printf("hey!\n");
    return FALSE;
}

static void grpmQueryAdd(QueryWindow *q, GrpmPackageListItem *item)
{
    GtkWidget *tableLabel, *label, *entry, *frame, *desc;
    GtkWidget *scroll, *vbox, *hbox, *scr_win;
    GtkTable *table;
    GtkCList *fileList;
    gchar *titles[] = { "C", "D", "File" };
    gchar **files;
    gint *flags;
    gint num = 0;
    gchar *row[3];
    gchar buf[BUFSIZ];
    gchar *s;
    gint *x;

    vbox = gtk_vbox_new(FALSE, 5);
    gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
    gtk_widget_show(vbox);
    
    tableLabel = gtk_label_new(item->pkg->name);
    gtk_widget_show(tableLabel);

    table = GTK_TABLE(gtk_table_new(0, 0, FALSE));
    gtk_table_set_homogeneous(GTK_TABLE(table), 0);
    gtk_container_set_border_width(GTK_CONTAINER(table), GNOME_PAD);

#define LABEL(name, col, row) { \
    label = gtk_label_new(name); \
    gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5); \
    gtk_table_attach(table, label, col, col + 1, row, row + 1, \
		     GTK_FILL, GTK_FILL, 5, 0); }

#define ENTRY(name, col, row) { \
    entry = gtk_label_new(name); \
    gtk_misc_set_alignment(GTK_MISC(entry), 0.0, 0.5); \
    gtk_table_attach(table, entry, col + 1, col + 2, row, row + 1, \
		     GTK_FILL, GTK_FILL, 5, 0); }

#define ENTRY_LONG(name, col, row) { \
    entry = gtk_label_new(name); \
    gtk_misc_set_alignment(GTK_MISC(entry), 0.0, 0.5); \
    gtk_table_attach(table, entry, col + 1, col + 4, row, row + 1, \
		     GTK_FILL, GTK_FILL, 5, 0); }

#define ENTRY_STRING(tag, col, row) { \
    if (!headerGetEntry(item->pkg->header, tag, NULL, (void **) &s, NULL)) { \
	ENTRY("(none)", col, row); \
    } else { \
	ENTRY(s, col, row); \
    } }

#define ENTRY_STRING_LONG(tag, col, row) { \
    if (!headerGetEntry(item->pkg->header, tag, NULL, (void **) &s, NULL)) { \
	ENTRY_LONG("(none)", col, row); \
    } else { \
	ENTRY_LONG(s, col, row); \
    } }

    /* First column */
    
    LABEL("Name:", 0, 0);
    ENTRY(item->pkg->name, 0, 0);

    LABEL("Version:", 0, 1);
    ENTRY_STRING(RPMTAG_VERSION, 0, 1);
    
    LABEL("Release:", 0, 2);
    ENTRY_STRING(RPMTAG_RELEASE, 0, 2);
    
    LABEL("Install date:", 0, 3);
    if (headerIsEntry(item->pkg->header, RPMTAG_INSTALLTIME)) {
	s = headerSprintf(item->pkg->header, "%{INSTALLTIME:date}",
			  rpmTagTable, rpmHeaderFormats, NULL);
	ENTRY(s, 0, 3);
	free(s);
    } else {
	ENTRY("(not installed)", 0, 3);
    }
    
    LABEL("Group:", 0, 4);
    ENTRY_STRING(RPMTAG_GROUP, 0, 4);

    LABEL("Size:", 0, 5);
    headerGetEntry(item->pkg->header, RPMTAG_SIZE, NULL, (void **) &x, NULL);
    g_snprintf(buf, sizeof(buf), "%d", *x);
    ENTRY(buf, 0, 5);

    LABEL("Packager:", 0, 6);
    ENTRY_STRING_LONG(RPMTAG_PACKAGER, 0, 6);

    LABEL("Summary:", 0, 7);
    ENTRY_STRING_LONG(RPMTAG_SUMMARY, 0, 7);

    /* Second column */
    
    LABEL("Distribution:", 2, 0);
    ENTRY_STRING(RPMTAG_DISTRIBUTION, 2, 0);

    LABEL("Vendor:", 2, 1);
    ENTRY_STRING(RPMTAG_VENDOR, 2, 1);
    
    LABEL("Build Date:", 2, 2);
    s = headerSprintf(item->pkg->header, "%{BUILDTIME:date}", rpmTagTable,
		      rpmHeaderFormats, NULL);
    ENTRY(s, 2, 2);
    free(s);
    
    LABEL("Build Host:", 2, 3);
    ENTRY_STRING(RPMTAG_BUILDHOST, 2, 3);
    
    LABEL("Source RPM:", 2, 4);
    ENTRY_STRING(RPMTAG_SOURCERPM, 2, 4);

    /* Description */

    frame = gtk_frame_new("Description");
    hbox = gtk_hbox_new(0, 5);
    gtk_container_add(GTK_CONTAINER(frame), hbox);
    
    desc = gtk_text_new(FALSE, FALSE);
    gtk_text_set_editable(GTK_TEXT(desc), FALSE);
    gtk_text_set_word_wrap(GTK_TEXT(desc), TRUE);
    gtk_box_pack_start(GTK_BOX(hbox), desc, TRUE, TRUE, 5);

    scroll = gtk_vscrollbar_new(GTK_TEXT(desc)->vadj);
    gtk_signal_connect(GTK_OBJECT(GTK_TEXT(desc)->vadj), "changed",
		       (GtkSignalFunc) grpmQuerySetScroll, scroll);
    gtk_box_pack_end(GTK_BOX(hbox), scroll, FALSE, 0, 5);

    /* File list */

    gtk_widget_push_visual(gdk_imlib_get_visual());
    gtk_widget_push_colormap(gdk_imlib_get_colormap());
    fileList = GTK_CLIST(gtk_clist_new_with_titles(3, titles));
    gtk_widget_pop_colormap();
    gtk_widget_pop_visual();

    gtk_clist_column_titles_passive(fileList);

    gtk_clist_set_column_width(fileList, 0, 8);
    gtk_clist_set_column_width(fileList, 1, 8);
    
    headerGetEntry(item->pkg->header, RPMTAG_FILENAMES, NULL,
		   (void **)&files, &num);
    headerGetEntry(item->pkg->header, RPMTAG_FILEFLAGS, NULL,
		   (void **)&flags, NULL);

    gtk_clist_freeze(fileList);
    while (num--) {
	row[0] = (flags[num] & RPMFILE_CONFIG) ? "C" : "";
	row[1] = (flags[num] & RPMFILE_DOC) ? "D" : "";
	row[2] = files[num];
	gtk_clist_insert(fileList, 0, row);
    }
    gtk_clist_thaw(fileList);
    free(files);
    
    /* put that clist in a scrolled window */

    scr_win = gtk_scrolled_window_new (NULL, NULL);
    gtk_container_add (GTK_CONTAINER(scr_win),
                       GTK_WIDGET(fileList));
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(scr_win),
                                    GTK_POLICY_AUTOMATIC,
                                    GTK_POLICY_AUTOMATIC);

    gtk_widget_show(scr_win);

    /* Done */

    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(table), FALSE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
    gtk_widget_set_usize(frame, -1, 100);
    gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(scr_win), TRUE, TRUE, 0);
    gtk_widget_set_usize(GTK_WIDGET(fileList), -1, 150);

    gtk_widget_show_all(GTK_WIDGET(vbox));
    
    gtk_notebook_append_page(GTK_NOTEBOOK(q->notebook), vbox, tableLabel);

    /* Have to do this here for some goofy GTK reason */
    
    headerGetEntry(item->pkg->header, RPMTAG_DESCRIPTION, NULL,
		   (void **) &s, NULL);
    gtk_text_freeze(GTK_TEXT(desc));
    gtk_widget_realize(GTK_WIDGET(desc));
    gtk_text_insert(GTK_TEXT(desc), NULL, NULL, NULL, s, -1);
    gtk_text_thaw(GTK_TEXT(desc));
}

static void grpmQuerySetScroll(GtkAdjustment *adj, GtkWidget *scroll)
{
    if (adj->upper - adj->lower <= adj->page_size) {
	gtk_widget_hide(scroll);
    } else {
	gtk_widget_show(scroll);
    }
}
