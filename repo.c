#include "grpm.h"

#define USEZLIB 1

#include "stringbuf.h"
#include "misc.h"
#include "ftp.h"

#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

gboolean grpmRepoArchOsMatch(Header h);

static gint grpmRepoLoadCache(GrpmRepo *repo, gint *count);
static void grpmRepoAddPackage(GrpmRepo *repo, gchar *ref, Header h);
static gint grpmRepoSyncCacheWriteEntry(GrpmPackageListItem *item, gpointer x);
static gint grpmRepoLooksLikeRpm(gchar *file);
static void grpmRepoSetPackageState(GrpmPackage *pkg, GrpmRpmdb *rdb);

static gint grpmRepoInitLocal(GrpmRepo *repo);
static gint grpmRepoLocalOpenSession(GrpmRepo *repo);
static void grpmRepoLocalCloseSession(GrpmRepo *repo);
static gchar **grpmRepoLocalListFiles(GrpmRepo *repo);
static Header grpmRepoLocalGetHeader(GrpmRepo *repo, gchar *file,
				     gint *isSource);
static gint grpmRepoLocalStatFile(GrpmRepo *repo, gchar *file);

static gint grpmRepoInitFtp(GrpmRepo *repo);
static gint grpmRepoFtpOpenSession(GrpmRepo *repo);
static void grpmRepoFtpCloseSession(GrpmRepo *repo);
static gchar **grpmRepoFtpListFiles(GrpmRepo *repo);
static Header grpmRepoFtpGetHeader(GrpmRepo *repo, gchar *file,
				     gint *isSource);
static gint grpmRepoFtpStatFile(GrpmRepo *repo, gchar *file);
static gint grpmRepoFtpGetFileToFd(GrpmRepo *repo, gchar *ref, int fd);

static gint grpmRepoInitHttp(GrpmRepo *repo);
static gint grpmRepoHttpOpenSession(GrpmRepo *repo);
static void grpmRepoHttpCloseSession(GrpmRepo *repo);
static gchar **grpmRepoHttpListFiles(GrpmRepo *repo);
static Header grpmRepoHttpGetHeader(GrpmRepo *repo, gchar *file,
				    gint *isSource);
static gint grpmRepoHttpStatFile(GrpmRepo *repo, gchar *file);
static gint grpmRepoHttpGetFileToFd(GrpmRepo *repo, gchar *ref, int fd);
static int getHostByName(const char *host, struct in_addr *address);
static int grpmRepoHttpOpenCommand(GrpmRepo *repo, gchar *cmd);

#define SED(s, b, a) \
{ \
    gchar *p = s; \
    while (*p) { \
	if (*p == b) { \
	    *p = a; \
	} \
	p++; \
    } \
}

GrpmRepo *grpmRepoNew(GrpmRepoType type, gchar *ref, gchar *cache,
		      GrpmRpmdb *installedRpmdb, gboolean isRemovable,
		      gchar *name)
{
    GrpmRepo *res;
    gchar buf[BUFSIZ];
    gint count;

    res = g_new(GrpmRepo, 1);
    res->list = grpmPackageListNew();
    res->name = g_strdup(name);
    res->type = type;
    res->ref = g_strdup(ref);
    res->installedRpmdb = installedRpmdb;
    res->isRemovable = isRemovable;

    if (cache && *cache) {
	res->cache = g_strdup(cache);
    } else {
	/* Generate a cache file name */
	g_snprintf(buf, sizeof(buf), "%s%s", ref, name);
	SED(buf, '/', '_');
	/* That should be enough, but for kicks we'll get rid of these too */
	SED(buf, ' ', '_');
	SED(buf, '"', '_');
	SED(buf, '\'', '_');
	SED(buf, '%', '_');
	SED(buf, '$', '_');
	SED(buf, '*', '_');
	SED(buf, '(', '_');
	SED(buf, ')', '_');
	SED(buf, '?', '_');
	SED(buf, ';', '_');
	res->cache = g_strdup(buf);
    }
    
    if (grpmRepoLoadCache(res, &count)) {
	g_warning("Cache load failed, %s, %s", ref, cache);
    } else {
	g_message("Cache %s (%s): %d loaded", ref, cache, count);
    }

    res->host = NULL;
    res->user = NULL;
    res->pass = NULL;
    res->path = NULL;
    
    switch (res->type) {
      case GRPM_REPO_LOCAL:
	grpmRepoInitLocal(res);
	break;
      case GRPM_REPO_FTP:
	grpmRepoInitFtp(res);
	break;
      case GRPM_REPO_HTTP:
	grpmRepoInitHttp(res);
	break;
      default:
	g_message("Unknown repo type %d", res->type);
	grpmRepoDestroy(res);
	res = NULL;
    }

    return res;
}

void grpmRepoDestroy(GrpmRepo *repo)
{
    grpmRepoSyncCache(repo);
    
    GFREE(repo->name);
    GFREE(repo->ref);
    GFREE(repo->cache);

    GFREE(repo->host);
    GFREE(repo->user);
    GFREE(repo->pass);
    GFREE(repo->path);

    grpmPackageListFree(repo->list, TRUE);

    g_free(repo);
}

void grpmRepoResetPackageStates(GrpmRepo *repo)
{
    GList *list;
    GrpmPackageListItem *listItem;

    list = repo->list->packages;
    while (list) {
	listItem = (GrpmPackageListItem *) list->data;
	grpmRepoSetPackageState(listItem->pkg, repo->installedRpmdb);
	list = list->next;
    }
}

gboolean grpmRepoArchOsMatch(Header h)
{
    char *arch;
    char *os;

    headerGetEntry(h, RPMTAG_ARCH, NULL, (void **) &arch, NULL);
    headerGetEntry(h, RPMTAG_OS, NULL, (void **) &os, NULL);
    if (!rpmMachineScore(RPM_MACHTABLE_INSTARCH, arch)) {
	return FALSE;
    }
    if (!rpmMachineScore(RPM_MACHTABLE_INSTOS, os)) {
	return FALSE;
    }
    
    return TRUE;
}

gint grpmRepoScan(GrpmRepo *repo)
{
    gchar **files, **filesSave;
    Header h;
    gint isSource;
    gint newCount, removedCount;
    GrpmPackageListItem *listItem;

    newCount = 0;
    removedCount = 0;
    
    /* In this mode we go through the list we have, removing packages */
    /* that no longer exist, and adding packages we don't have.       */

    /* Unmark all packages in list */
    grpmPackageListUnset(repo->list, GRPM_PACKAGE_FLAG_MARKED);
    
    /* Do the scan, marking packages */
    if (repo->openSession(repo)) {
	return 1;
    }

    g_message("Scanning %s ...", repo->ref);
    
    files = repo->listFiles(repo);
    if (files) {
	filesSave = files;
	while (*files) {
	    if (! grpmRepoLooksLikeRpm(*files)) {
		files++;
		continue;
	    }
	    if ((listItem =
		 grpmPackageListEntryFindByRef(repo->list, *files))) {
		grpmPackageListItemSet(listItem, GRPM_PACKAGE_FLAG_MARKED);
		files++;
		continue;
	    }
	    h = repo->getHeader(repo, *files, &isSource);
	    if (h) {
		if (! isSource) {
		    /* grpmRepoAddPackage() sets MARKED */
		    if (grpmRepoArchOsMatch(h)) {
			grpmRepoAddPackage(repo, *files, h);
			newCount++;
		    }
		}
		headerFree(h);
	    }
	    files++;
	}
	freeSplitString(filesSave);
    }
    repo->closeSession(repo);

    /* Remove all unmarked packages in list */
    removedCount = grpmPackageListRemoveUnset(repo->list,
					      GRPM_PACKAGE_FLAG_MARKED,
					      TRUE);

    /* Write the cache */
    grpmRepoSyncCache(repo);

    g_message("Scanned %s: %d new, %d removed",
	      repo->ref, newCount, removedCount);

    return 0;
}

static gint grpmRepoLooksLikeRpm(gchar *file)
{
    gint len;
    gchar *s;

    len = strlen(file);
    if (len < 4) {
	return 0;
    }

    s = file + len - 4;
    if (strcmp(s, ".rpm")) {
	return 0;
    }

    return 1;
}

gint grpmRepoReload(GrpmRepo *repo)
{
    /* Toss existing list */
    grpmPackageListFree(repo->list, TRUE);
    repo->list = grpmPackageListNew();

    /* Just scan again */
    return grpmRepoScan(repo);
}

gint grpmRepoOpenSession(GrpmRepo *repo)
{
    return (repo->openSession(repo));
}

void grpmRepoCloseSession(GrpmRepo *repo)
{
    repo->closeSession(repo);
}

gint grpmRepoSyncCache(GrpmRepo *repo)
{
#ifdef USEZLIB    
    gzFile fd;
#else
    int fd;
#endif    
    gchar buf[BUFSIZ];
    struct stat statb;

    errno = 0;
    g_snprintf(buf, sizeof(buf), "%s/.grpm", getenv("HOME"));
    if (stat(buf, &statb)) {
	if (mkdir(buf, 0755)) {
	    g_warning("Unable to mkdir $HOME/.grpm: %s", strerror(errno));
	    return 1;
	}
    }

    errno = 0;
    g_snprintf(buf, sizeof(buf), "%s/.grpm/%s", getenv("HOME"), repo->cache);
#ifdef USEZLIB    
    fd = gzopen(buf, "w");
    if (!fd) {
	g_warning("Unable to open %s: %s", buf, strerror(errno));
	return 1;
    }
#else
    fd = open(buf, O_WRONLY | O_CREAT | O_TRUNC, 0600);
    if (fd < 0) {
	g_warning("Unable to open %s: %s", buf, strerror(errno));
	return 1;
    }
#endif    

    g_list_foreach(repo->list->packages,
		   (GFunc) grpmRepoSyncCacheWriteEntry,
		   (gpointer) fd);

#ifdef USEZLIB    
    gzclose(fd);
#else
    close(fd);
#endif    

    return 0;
}

gint grpmRepoGetPackageFileName(GrpmRepo *repo, GrpmPackage *pkg,
				gchar *fileName)
{
    char *s;
    int fd;

    if (repo->type == GRPM_REPO_LOCAL) {
	g_snprintf(fileName, sizeof(fileName), "%s/%s", repo->ref, pkg->ref);
	return 0;
    }
    
    makeTempFile(NULL, &s, &fd);
    strcpy(fileName, s);
    free(s);

    if (repo->openSession(repo)) {
	close(fd);
	unlink(fileName);
	return 1;
    }
    
    if (repo->getFileToFd(repo, pkg->ref, fd)) {
	close(fd);
	unlink(fileName);
	return 1;
    }
    close(fd);

    repo->closeSession(repo);

    return 0;
}

static int_32 interestingHeaderTags[] = {
    RPMTAG_NAME,
    RPMTAG_VERSION,
    RPMTAG_RELEASE,
    RPMTAG_SERIAL,
    RPMTAG_SUMMARY,
    RPMTAG_DESCRIPTION,
    RPMTAG_BUILDTIME,
    RPMTAG_BUILDHOST,
    RPMTAG_INSTALLTIME,
    RPMTAG_SIZE,
    RPMTAG_ARCHIVESIZE,
    RPMTAG_DISTRIBUTION,
    RPMTAG_VENDOR,
    RPMTAG_COPYRIGHT,
    RPMTAG_PACKAGER,
    RPMTAG_SOURCERPM,
    RPMTAG_GROUP,
    RPMTAG_URL,

    RPMTAG_FILENAMES,
    RPMTAG_FILEMD5S,
    RPMTAG_FILESIZES,
    RPMTAG_FILESTATES,
    RPMTAG_FILEMODES,
    RPMTAG_FILERDEVS,
    RPMTAG_FILEMTIMES,
    RPMTAG_FILELINKTOS,
    RPMTAG_FILEFLAGS,
    RPMTAG_FILEDEVICES,
    RPMTAG_FILEINODES,
    RPMTAG_FILELANGS,
    RPMTAG_FILEUSERNAME,
    RPMTAG_FILEGROUPNAME,
    
    RPMTAG_PROVIDES,
    RPMTAG_REQUIREFLAGS,
    RPMTAG_REQUIRENAME,
    RPMTAG_REQUIREVERSION,
    RPMTAG_CONFLICTFLAGS,
    RPMTAG_CONFLICTNAME,
    RPMTAG_CONFLICTVERSION,
    RPMTAG_EXCLUDEARCH,
    RPMTAG_EXCLUDEOS,
    RPMTAG_EXCLUSIVEARCH,
    RPMTAG_EXCLUSIVEOS,
    RPMTAG_OBSOLETES,
    
    0
};

Header grpmRepoCopyInterestingHeaderBits(Header h)
{
    int_32 *tag = interestingHeaderTags;
    int_32 type, count;
    void *ptr;
    Header res;

    res = headerNew();

    while (*tag) {
	if (headerGetEntry(h, *tag, &type, &ptr, &count)) {
	    headerAddEntry(res, *tag, type, ptr, count);
	}

	if (type == RPM_STRING_ARRAY_TYPE ||
	    type == RPM_I18NSTRING_TYPE) {
	    free(ptr);
	}
	       
	tag++;
    }

    return res;
}

/***************************************************************************/

/* Misc static */

static void grpmRepoSetPackageState(GrpmPackage *pkg, GrpmRpmdb *rdb)
{
    int x;
    
    /* XXX should handle obsoletes et al */
    
    GrpmPackageListItem *installedPkg;
    
    installedPkg = grpmPackageListEntryFindByName(rdb->list, pkg->name);
    if (! installedPkg) {
	pkg->state = GRPM_PACKAGE_STATE_UNINSTALLED;
    } else {
	/* Some version of the package is installed */
	x = rpmVersionCompare(pkg->header, installedPkg->pkg->header);
	if (x > 0) {
	    pkg->state = GRPM_PACKAGE_STATE_NEWPACKAGE;

            /* Set installed package to be "old" */
	    installedPkg->pkg->state = GRPM_PACKAGE_STATE_OLDPACKAGE;
	} else if (x < 0) {
	    pkg->state = GRPM_PACKAGE_STATE_OLDPACKAGE;
	} else {
	    pkg->state = GRPM_PACKAGE_STATE_INSTALLED;
	}
    }
}

static void grpmRepoAddPackage(GrpmRepo *repo, gchar *ref, Header h)
{
    GrpmPackage *pkg;
    GrpmPackageListItem *pkgItem;
    gchar *name;

    pkg = g_new(GrpmPackage, 1);

    pkg->header = grpmRepoCopyInterestingHeaderBits(h);
    pkg->ref = g_strdup(ref);
    pkg->repo = repo;
    pkg->doWhat = GRPM_PACKAGE_DOWHAT_NOTHING;
    headerGetEntry(pkg->header, RPMTAG_NAME, NULL, (void **)&name, NULL);
    pkg->name = g_strdup(name);
    
    if (repo->installedRpmdb) {
	grpmRepoSetPackageState(pkg, repo->installedRpmdb);
    } else {
	pkg->state = GRPM_PACKAGE_STATE_UNKNOWN;
    }

    pkgItem = grpmPackageListAddPackage(repo->list, pkg);
    grpmPackageListItemSet(pkgItem, GRPM_PACKAGE_FLAG_MARKED);
}

static gint grpmRepoLoadCache(GrpmRepo *repo, gint *count)
{
    gchar buf[BUFSIZ];
    gchar name[BUFSIZ];
    Header h;
    int c;
#ifdef USEZLIB    
    gzFile fd;
#else
    int fd;
#endif    

    *count = 0;
    
    g_message("Loading cache for %s ...", repo->name);
    
    errno = 0;
    g_snprintf(buf, sizeof(buf), "%s/.grpm/%s", getenv("HOME"), repo->cache);
#ifdef USEZLIB    
    fd = gzopen(buf, "r");
    if (!fd) {
	g_warning("Unable to open %s: %s", buf, strerror(errno));
	return 1;
    }
#else
    fd = open(buf, O_RDONLY);
    if (fd < 0) {
	return 1;
    }
#endif

#ifdef USEZLIB    
    while (gzread(fd, name, 1)) {
#else
    while (read(fd, name, 1)) {
#endif	
	c = 0;
	while (name[c] != '\0') {
	    c++;
#ifdef USEZLIB    
	    gzread(fd, name + c, 1);
#else
	    read(fd, name + c, 1);
#endif	    
	}

	if ((h = headerGzRead(fd, HEADER_MAGIC_YES)) == NULL) {
	    g_warning("Failed reading cache header for %s", name);
	} else {
	    grpmRepoAddPackage(repo, name, h);
	    headerFree(h);
	    (*count)++;
	}
    }

#ifdef USEZLIB    
    gzclose(fd);
#else
    close(fd);
#endif    

    g_message("Loading cache for %s ... done", repo->name);

    return 0;
}

static gint grpmRepoSyncCacheWriteEntry(GrpmPackageListItem *item, gpointer x)
{
    gchar name[BUFSIZ];
#ifdef USEZLIB    
    gzFile fd;
#else
    gint fd;
#endif    

    g_snprintf(name, sizeof(name), item->pkg->ref);
#ifdef USEZLIB    
    fd = (gzFile) x;
    gzwrite(fd, name, strlen(name) + 1);
    headerGzWrite(fd, item->pkg->header, HEADER_MAGIC_YES);
#else
    fd = (gint) x;
    write(fd, name, strlen(name) + 1);
    headerWrite(fd, item->pkg->header, HEADER_MAGIC_YES);
#endif    

    return 0;
}

/***************************************************************************/

/* LOCAL */

static gint grpmRepoInitLocal(GrpmRepo *repo)
{
    repo->openSession = grpmRepoLocalOpenSession;
    repo->closeSession = grpmRepoLocalCloseSession;
    repo->listFiles = grpmRepoLocalListFiles;
    repo->getHeader = grpmRepoLocalGetHeader;
    repo->statFile = grpmRepoLocalStatFile;
    repo->getFileToFd = NULL;
    
    return 0;
}

static gint grpmRepoLocalOpenSession(GrpmRepo *repo)
{
    gchar buf[BUFSIZ];
    
    if (repo->isRemovable) {
	if (access(repo->ref, R_OK)) {
	    g_snprintf(buf, sizeof(buf), "Unable to access removable repo:\n\n%s\n\n"
		    "Please ensure it is mounted and try\n"
		    "this operation again.", repo->name);
	    gnome_error_dialog(buf);
	    return 1;
	}
    }
    
    return 0;
}

static void grpmRepoLocalCloseSession(GrpmRepo *repo)
{
    return;
}

static gchar **grpmRepoLocalListFiles(GrpmRepo *repo)
{
    DIR *dir;
    struct dirent *dent;
    struct stat statb;
    gchar filename[BUFSIZ];
    StringBuf sb;
    gchar *s;
    gchar **res;

    if (! (dir = opendir(repo->ref))) {
	g_warning("Unable to open %s", repo->ref);
	return NULL;
    }

    sb = newStringBuf();

    while ((dent = readdir(dir))) {
	g_snprintf(filename, sizeof(filename), "%s/%s", repo->ref, dent->d_name);

	errno = 0;
	if (stat(filename, &statb)) {
	    g_warning("Unable to stat %s: %s", filename, strerror(errno));
	    continue;
	}

	if (!S_ISREG(statb.st_mode) || !statb.st_size) {
	    continue;
	}

	appendLineStringBuf(sb, dent->d_name);
    }

    closedir(dir);

    s = getStringBuf(sb);
    res = splitString(s, strlen(s), '\n');
    freeStringBuf(sb);

    return res;
}

static Header grpmRepoLocalGetHeader(GrpmRepo *repo, gchar *file,
				     gint *isSource)
{
    gchar filename[BUFSIZ];
    struct stat sb;
    int fd;
    Header h;

    g_snprintf(filename, sizeof(filename), "%s/%s", repo->ref, file);

    errno = 0;
    if (stat(filename, &sb)) {
	g_warning("Unable to stat %s: %s", filename, strerror(errno));
	return NULL;
    }

    if (!S_ISREG(sb.st_mode) || !sb.st_size) {
	return NULL;
    }
	    
    fd = open(filename, O_RDONLY);
    if (fd < 0) {
	g_warning("Unable to open %s: %s", filename, strerror(errno));
	return NULL;
    }

    if (!rpmReadPackageHeader(fd, &h, isSource, NULL, NULL)) {
	close(fd);
	return h;
    }

    g_warning("Failed to read package header: %s", filename);
    return NULL;
}

static gint grpmRepoLocalStatFile(GrpmRepo *repo, gchar *file)
{
    gchar filename[BUFSIZ];

    g_snprintf(filename, sizeof(filename), "%s/%s", repo->ref, file);
    return access(filename, R_OK);
}

/***************************************************************************/

/* FTP */

static gint grpmRepoInitFtp(GrpmRepo *repo)
{
    gchar buf[BUFSIZ];
    gchar *s;
    
    repo->openSession = grpmRepoFtpOpenSession;
    repo->closeSession = grpmRepoFtpCloseSession;
    repo->listFiles = grpmRepoFtpListFiles;
    repo->getHeader = grpmRepoFtpGetHeader;
    repo->statFile = grpmRepoFtpStatFile;
    repo->getFileToFd = grpmRepoFtpGetFileToFd;

    /* XXX right now we assume a "perfect" ftp URL */
    strcpy(buf, repo->ref);
    s = repo->host = buf + 6;
    s = strchr(s, '/');
    *s = '\0';
    repo->host = g_strdup(repo->host);
    repo->path = s + 1;
    repo->path = g_strdup(repo->path);
    
    return 0;
}

static gint grpmRepoFtpOpenSession(GrpmRepo *repo)
{
    repo->sock = ftpOpen(repo->host,
			 repo->user ? repo->user : "anonymous",
			 repo->pass ? repo->pass : "grpm@localhost",
			 NULL, -1);

    if (repo->sock < 0) {
	g_warning("ftp open failed: %s: %s", repo->host,
		  ftpStrerror(repo->sock));
	return repo->sock;
    }

    return 0;
}

static void grpmRepoFtpCloseSession(GrpmRepo *repo)
{
    ftpClose(repo->sock);
}

static gchar **grpmRepoFtpListFiles(GrpmRepo *repo)
{
    StringBuf sb;
    gchar *s;
    gchar **res, **p;
    gchar buf[BUFSIZ];
    gint skip;

    sb = newStringBuf();

    ftpGetDir(repo->sock, repo->path, sb);

    s = getStringBuf(sb);
    res = splitString(s, strlen(s), '\n');
    /*freeStringBuf(sb);*/

    /* This is pretty stupid */
    p = res;
    skip = strlen(repo->path) + 1;
    while (*p) {
	if (**p) {
	    strcpy(buf, *p);
	    strcpy(*p, buf + skip);
	}
	p++;
    }
    
    return res;
}

static Header grpmRepoFtpGetHeader(GrpmRepo *repo, gchar *file,
				   gint *isSource)
{
    int fd;
    gint rc;
    gchar *s;
    gchar tempname[BUFSIZ];
    gchar filename[BUFSIZ];
    Header h;

    makeTempFile(NULL, &s, &fd);
    strcpy(tempname, s);
    free(s);
    
    g_snprintf(filename, sizeof(filename), "%s/%s", repo->path, file);
    rc = ftpGetFile(repo->sock, filename, fd);
    close(fd);

    if (rc) {
	g_warning("Failed to get file: %s/%s: %s",
		  repo->ref, file, ftpStrerror(rc));
	unlink(tempname);
	return NULL;
    }
    
    fd = open(tempname, O_RDONLY);
    if (!rpmReadPackageHeader(fd, &h, isSource, NULL, NULL)) {
	close(fd);
	unlink(tempname);
	return h;
    }
    
    close(fd);
    unlink(tempname);

    g_warning("Failed to read package header: %s/%s", repo->ref, file);
    return NULL;
}

static gint grpmRepoFtpGetFileToFd(GrpmRepo *repo, gchar *ref, int fd)
{
    gint rc = 0;
    gchar filename[BUFSIZ];

    g_snprintf(filename, sizeof(filename), "%s/%s", repo->path, ref);
    if ((rc = ftpGetFile(repo->sock, filename, fd))) {
	g_warning("Failed to get file: %s: %s", filename, ftpStrerror(rc));
    }

    return rc;
}

static gint grpmRepoFtpStatFile(GrpmRepo *repo, gchar *file)
{
    StringBuf sb;
    gchar *s;
    gchar filename[BUFSIZ];
    gint res;

    sb = newStringBuf();

    g_snprintf(filename, sizeof(filename), "%s/%s", repo->path, file);
    ftpGetDir(repo->sock, filename, sb);

    s = getStringBuf(sb);
    res = (*s) ? 0 : 1;
    freeStringBuf(sb);

    return res;
}

/***************************************************************************/

/* HTTP */

static gint grpmRepoInitHttp(GrpmRepo *repo)
{
    gchar buf[BUFSIZ];
    gchar *s;
    
    repo->openSession = grpmRepoHttpOpenSession;
    repo->closeSession = grpmRepoHttpCloseSession;
    repo->listFiles = grpmRepoHttpListFiles;
    repo->getHeader = grpmRepoHttpGetHeader;
    repo->statFile = grpmRepoHttpStatFile;
    repo->getFileToFd = grpmRepoHttpGetFileToFd;
    
    /* XXX right now we assume a "perfect" http URL */
    strcpy(buf, repo->ref);
    s = repo->host = buf + 7;
    s = strchr(s, '/');
    *s = '\0';
    repo->host = g_strdup(repo->host);
    repo->path = s + 1;
    repo->path = g_strdup(repo->path);
    
    return 0;
}

static gint grpmRepoHttpOpenSession(GrpmRepo *repo)
{
    return 0;
}

static void grpmRepoHttpCloseSession(GrpmRepo *repo)
{
    return;
}

static gchar **grpmRepoHttpListFiles(GrpmRepo *repo)
{
    gchar buf[BUFSIZ];
    gint sock;
    gchar *outbuf;
    gint outbuflen;
    gint bytes;
    gchar **res;
    
    g_snprintf(buf, sizeof(buf), "GET /%s/ HTTP/1.0\n\n", repo->path);

    sock = grpmRepoHttpOpenCommand(repo, buf);
    if (!sock) {
	g_warning("Unable to open %s", repo->ref);
	return NULL;
    }

    outbuf = NULL;
    outbuflen = 0;
    while ((bytes = read(sock, buf, sizeof(buf))) > 0) {
	outbuf = g_realloc(outbuf, outbuflen + bytes + 1);
	memcpy(outbuf + outbuflen, buf, bytes);
	outbuflen += bytes;
    }
    close(sock);

    outbuf[outbuflen] = '\0';
    res = splitString(outbuf, strlen(outbuf), '\n');

    return res;
}

static Header grpmRepoHttpGetHeader(GrpmRepo *repo, gchar *file,
				    gint *isSource)
{
    gchar buf[BUFSIZ];
    gint sock;
    Header h;
    
    g_snprintf(buf, sizeof(buf), "GET /%s/%s HTTP/1.0\n\n", repo->path, file);

    sock = grpmRepoHttpOpenCommand(repo, buf);
    if (!sock) {
	return NULL;
    }
    
    if (rpmReadPackageHeader(sock, &h, isSource, NULL, NULL)) {
	g_warning("Failed to read package header: %s/%s", repo->ref, file);
	close(sock);
	return NULL;
    }

    close(sock);
    return h;
}

static gint grpmRepoHttpGetFileToFd(GrpmRepo *repo, gchar *ref, int fd)
{
    gchar buf[BUFSIZ];
    gint sock, bytes;
    
    g_snprintf(buf, sizeof(buf), "GET /%s/%s HTTP/1.0\n\n", repo->path, ref);

    sock = grpmRepoHttpOpenCommand(repo, buf);
    if (!sock) {
	return 1;
    }

    while ((bytes = read(sock, buf, sizeof(buf))) > 0) {
	write(fd, buf, bytes);
    }

    close(sock);

    return 0;
}

static gint grpmRepoHttpStatFile(GrpmRepo *repo, gchar *file)
{
    gchar **res;
    gchar **s;

    res = grpmRepoHttpListFiles(repo);
    if (! res) {
	freeSplitString(res);
	return 0;
    }

    s = res;
    while (*s) {
	if (!strcmp(*s, file)) {
	    freeSplitString(res);
	    return 1;
	}
	s++;
    }
    
    freeSplitString(res);
    return 0;
}

static int getHostByName(const char *host, struct in_addr *address)
{
    struct hostent *hostinfo;

    hostinfo = gethostbyname(host);
    if (!hostinfo)
	return 1;

    memcpy(address, hostinfo->h_addr_list[0], hostinfo->h_length);
    return 0;
}

static int grpmRepoHttpOpenCommand(GrpmRepo *repo, gchar *cmd)
{
    struct sockaddr_in destPort;
    struct in_addr serverAddress;
    int sock;
    gchar buf[BUFSIZ];
    gchar *s;
    
    if (getHostByName(repo->host, &serverAddress)) {
	g_message("unable to resolve host: %s", repo->host);
	return 0;
    }

    destPort.sin_family = AF_INET;
    destPort.sin_port = htons(80);
    destPort.sin_addr = serverAddress;

    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    errno = 0;
    if (connect(sock, (struct sockaddr *) &destPort, sizeof(destPort))) {
	g_warning("unable to connect to host: %s: %s",
		  repo->host, strerror(errno));
	close(sock);
	return 0;
    }

    write(sock, cmd, strlen(cmd));

    while (1) {
	/* read a line */
	s = buf;
	while (1) {
	    if (! read(sock, s, 1)) {
		g_warning("unexpected end of response: %s, %s",
			  repo->host, cmd);
		close(sock);
		return 0;
	    }
	    if (*s == '\n') {
		*s = '\0';
		break;
	    }
	    s++;
	}

	if ((buf[0] == '\0') || (buf[0] == '\r' && buf[1] == '\0')) {
	    break;
	}
    }

    /* We've read the entire HTTP header */

    return sock;
}
