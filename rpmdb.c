#include <fcntl.h>

#include "grpm.h"

static void grpmRpmdbAddPackage(GrpmRpmdb *rdb, int offset,
				gchar *ref, Header h);

GrpmRpmdb *grpmRpmdbNew(void)
{
    GrpmRpmdb *res;

    res = g_new(GrpmRpmdb, 1);
    res->list = grpmPackageListNew();
    res->name = NULL;

    grpmRpmdbScan(res);
    
    return res;
}

void grpmRpmdbDestroy(GrpmRpmdb *rdb)
{
    GFREE(rdb->name);

    grpmPackageListFree(rdb->list, TRUE);

    g_free(rdb);
}

void grpmRpmdbSetName(GrpmRpmdb *rdb, gchar *name)
{
    GFREE(rdb->name);
    rdb->name = g_strdup(name);
}

void grpmRpmdbResetPackageStates(GrpmRpmdb *rdb)
{
    GList *list;
    GrpmPackageListItem *listItem;

    list = rdb->list->packages;
    while (list) {
	listItem = (GrpmPackageListItem *) list->data;
	listItem->pkg->state = GRPM_PACKAGE_STATE_INSTALLED;
	list = list->next;
    }
}

gint grpmRpmdbReload(GrpmRpmdb *rdb)
{
    /* Toss existing list */
    grpmPackageListFree(rdb->list, TRUE);
    rdb->list = grpmPackageListNew();

    /* Just scan again */
    return grpmRpmdbScan(rdb);
}

gint grpmRpmdbScan(GrpmRpmdb *rdb)
{
    Header h;
    gint newCount, removedCount;
    GrpmPackageListItem *listItem;
    rpmdb db;
    int recnum;
    char *name, *version, *release;
    gchar buf[BUFSIZ];

    newCount = 0;
    removedCount = 0;
    
    /* In this mode we go through the list we have, removing packages */
    /* that no longer exist, and adding packages we don't have.       */

    if (rpmdbOpen("", &db, O_RDONLY, 0644)) {
	g_warning("Unable to open RPM database");
	gnome_error_dialog("Unable to open RPM database");
	return 1;
    }

    /* Unmark all packages in list */
    grpmPackageListUnset(rdb->list, GRPM_PACKAGE_FLAG_MARKED);
    
    /* Do the scan, marking packages */
    recnum = rpmdbFirstRecNum(db);
    while (recnum > 0) {
	if (!(h = rpmdbGetRecord(db, recnum))) {
	    g_warning("Error reading db package %d", recnum);
	    gnome_error_dialog("Error reading db package");
	    rpmdbClose(db);
	    return 1;
	}

	headerGetEntry(h, RPMTAG_NAME, NULL, (void **)&name, NULL);
	headerGetEntry(h, RPMTAG_VERSION, NULL, (void **)&version, NULL);
	headerGetEntry(h, RPMTAG_RELEASE, NULL, (void **)&release, NULL);
	snprintf(buf, sizeof(buf), "%s-%s-%s", name, version, release);

	if ((listItem = grpmPackageListEntryFindByRef(rdb->list, buf))) {
	    grpmPackageListItemSet(listItem, GRPM_PACKAGE_FLAG_MARKED);
	} else {
	    grpmRpmdbAddPackage(rdb, recnum, buf, h);
	    newCount++;
	}

	headerFree(h);
	
	recnum = rpmdbNextRecNum(db, recnum);
    }

    rpmdbClose(db);

    /* Remove all unmarked packages in list */
    removedCount = grpmPackageListRemoveUnset(rdb->list,
					      GRPM_PACKAGE_FLAG_MARKED,
					      TRUE);

    g_message("Scanned db: %d new, %d removed", newCount, removedCount);

    return 0;
}

/***************************************************************************/

/* Misc static */

static void grpmRpmdbAddPackage(GrpmRpmdb *rdb, int offset,
				gchar *ref, Header h)
{
    GrpmPackage *pkg;
    GrpmPackageListItem *pkgItem;
    gchar *name;

    pkg = g_new(GrpmPackage, 1);

    pkg->header = grpmRepoCopyInterestingHeaderBits(h);
    pkg->ref = g_strdup(ref);
    pkg->repo = NULL;
    pkg->offset = offset;
    pkg->state = GRPM_PACKAGE_STATE_INSTALLED;
    pkg->doWhat = GRPM_PACKAGE_DOWHAT_NOTHING;
    headerGetEntry(pkg->header, RPMTAG_NAME, NULL, (void **)&name, NULL);
    pkg->name = g_strdup(name);

    pkgItem = grpmPackageListAddPackage(rdb->list, pkg);
    grpmPackageListItemSet(pkgItem, GRPM_PACKAGE_FLAG_MARKED);
}
