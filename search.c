#include <string.h>

#include "grpm.h"

struct _GrpmSearchWindow {
    makeCollectionCB callback;
    GrpmCollection *collection;
    GtkWidget *upgrades;
    GtkWidget *new;
    GtkWidget *filep;
    GtkWidget *keywordp;
    GtkWidget *file;
    GtkWidget *keyword;
    GtkWidget *name;
};

typedef struct _GrpmSearchWindow GrpmSearchWindow;

static void grpmSearchWindowOK(GtkWidget *button, GrpmSearchWindow *search);
static void grpmSearchWindowCancel(GtkWidget *button,
				   GrpmSearchWindow *search);
static void grpmSearchTest(GrpmPackageListItem *listItem,
			   GrpmSearchWindow *search);

void grpmSearchWindowNew(makeCollectionCB callback)
{
    GrpmSearchWindow *search;
    GnomeDialog *window;
    GtkBox *vbox;
    GtkTable *table;
    GtkWidget *label;

    search = g_new(GrpmSearchWindow, 1);
    search->callback = callback;

    window = GNOME_DIALOG(gnome_dialog_new("Search",
					   GNOME_STOCK_BUTTON_OK,
					   GNOME_STOCK_BUTTON_CANCEL,
					   NULL));
    /*gtk_window_set_policy(GTK_WINDOW(dialog), TRUE, TRUE, FALSE);*/
    vbox = GTK_BOX(GNOME_DIALOG(window)->vbox);

    gnome_dialog_set_close(window, TRUE);
    gnome_dialog_button_connect(window, 0,
				(GtkSignalFunc)grpmSearchWindowOK, search);
    gnome_dialog_button_connect(window, 1,
				(GtkSignalFunc)grpmSearchWindowCancel, search);

    table = GTK_TABLE(gtk_table_new(0, 0, FALSE));
    gtk_table_set_homogeneous(GTK_TABLE(table), 0);
    gtk_container_set_border_width(GTK_CONTAINER(table), GNOME_PAD);
    gtk_box_pack_start(vbox, GTK_WIDGET(table), TRUE, TRUE, 5);

    search->upgrades = gtk_check_button_new_with_label("Upgradeable packages");
    gtk_table_attach(table, search->upgrades,
		     0, 1, 0, 1,
		     GTK_FILL, GTK_FILL, 5, 0);

    search->new = gtk_check_button_new_with_label("New packages");
    gtk_table_attach(table, search->new,
		     0, 1, 1, 2,
		     GTK_FILL, GTK_FILL, 5, 0);

    search->filep = gtk_check_button_new_with_label("Contains file");
    gtk_table_attach(table, search->filep,
		     0, 1, 2, 3,
		     GTK_FILL, GTK_FILL, 5, 0);

    search->file = gtk_entry_new();
    gtk_table_attach(table, search->file,
		     1, 2, 2, 3,
		     GTK_FILL, GTK_FILL, 5, 0);

    search->keywordp = gtk_check_button_new_with_label("Keyword");
    gtk_table_attach(table, search->keywordp,
		     0, 1, 3, 4,
		     GTK_FILL, GTK_FILL, 5, 0);

    search->keyword = gtk_entry_new();
    gtk_table_attach(table, search->keyword,
		     1, 2, 3, 4,
		     GTK_FILL, GTK_FILL, 5, 0);

    label = gtk_label_new("Name for Results Panel");
    gtk_table_attach(table, label,
		     0, 1, 4, 5,
		     GTK_FILL, GTK_FILL, 5, 0);
    
    search->name = gtk_entry_new();
    gtk_table_attach(table, search->name,
		     1, 2, 4, 5,
		     GTK_FILL, GTK_FILL, 5, 0);

    gtk_widget_show_all(GTK_WIDGET(window));
}

static void grpmSearchWindowCancel(GtkWidget *button, GrpmSearchWindow *search)
{
    g_free(search);
}

static void grpmSearchWindowOK(GtkWidget *button, GrpmSearchWindow *search)
{
    GList *panelp;
    GrpmPanel *panel;
    gchar *s;

    s = gtk_entry_get_text(GTK_ENTRY(search->name));
    if (!s || !*s) {
	s = "Search";
    }
    
    search->collection = grpmCollectionNew(s);

    /* for each match, grpmCollectionAddPackage(collection, listItem) */
    panelp = panelList;
    while (panelp) {
	panel = (GrpmPanel *)panelp->data;
	if (panel->repo) {
	    g_list_foreach(panel->list->packages,
			   (GFunc)grpmSearchTest, search);
	}
	panelp = panelp->next;
    }

    (search->callback)(search->collection);
    g_free(search);
}

static gint searchTags[] = {
    RPMTAG_NAME,
    RPMTAG_VERSION,
    RPMTAG_RELEASE,
    RPMTAG_SUMMARY,
    RPMTAG_DESCRIPTION,
    RPMTAG_BUILDHOST,
    RPMTAG_DISTRIBUTION,
    RPMTAG_VENDOR,
    RPMTAG_PACKAGER,
    RPMTAG_GROUP,
    RPMTAG_PROVIDES,
    RPMTAG_REQUIRENAME,
    0
};

static void grpmSearchTest(GrpmPackageListItem *listItem,
			   GrpmSearchWindow *search)
{
    gboolean pass = FALSE;
    char **files, **filesSave, *key, *s;
    int num;

    if (GTK_TOGGLE_BUTTON(search->filep)->active) {
	key = gtk_entry_get_text(GTK_ENTRY(search->file));
	if (key && *key) {
	    files = NULL;
	    headerGetEntry(listItem->pkg->header, RPMTAG_FILENAMES, NULL,
			   (void **)&files, &num);
	    if (files) {
		filesSave = files;
		while (num--) {
		    if (strstr(*files, key)) {
			pass = TRUE;
			break;
		    }
		    files++;
		}
		free(filesSave);
	    }
	}
    }

    if (GTK_TOGGLE_BUTTON(search->keywordp)->active) {
	key = gtk_entry_get_text(GTK_ENTRY(search->keyword));
	if (key && *key) {
	    num = 0;
	    while (searchTags[num]) {
		s = NULL;
		headerGetEntry(listItem->pkg->header, searchTags[num], NULL,
			       (void **)&s, NULL);
		if (s && strstr(s, key)) {
		    pass = TRUE;
		    break;
		}
		num++;
	    }
	}
    }
    
    if (GTK_TOGGLE_BUTTON(search->upgrades)->active) {
	if (listItem->pkg->state == GRPM_PACKAGE_STATE_NEWPACKAGE) {
	    pass = TRUE;
	}
    }
    
    if (GTK_TOGGLE_BUTTON(search->new)->active) {
	if (listItem->pkg->state == GRPM_PACKAGE_STATE_UNINSTALLED) {
	    pass = TRUE;
	}
    }
    
    if (pass) {
	grpmCollectionAddPackage(search->collection, listItem);
    }
}

